﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class AudioManager : MonoBehaviourSingleton<AudioManager>
{
    private AudioSource currentMusicSource;
    private List<AudioSource> currentSoundSources = new List<AudioSource>();
    private Dictionary<string, AudioClip> cachingSounds = new Dictionary<string, AudioClip>();
    private GameObject audioContainer;
    [SerializeField] private string cachingFolderPath = "Audio/Sounds/Сaching";
    [SerializeField] private string notCachingFolderPath = "Audio/Sounds/NotCaching/";
    [SerializeField] private string musicFolderPath = "Audio/Music/";
    private bool isActiv = false;

    void Awake()
    {
        Init();
    }

    public void SetAudioActive(bool state)
    {
        StartCoroutine(WaitMusicSourceLoad(state));
    }

    IEnumerator WaitMusicSourceLoad(bool state)
    {
        yield return new WaitUntil(() => (currentMusicSource != null && currentMusicSource.clip != null));
        isActiv = state;
        if (!isActiv)
            MusicVolume(0, 0);
        else
            MusicVolume(1, 0);
    }

    public void PlayMusic(string id, float volume = 1, float fadeDuration = 1)
    {
        if (string.IsNullOrEmpty(id))
        {
            Debug.Log("Music empty or null");
            return;
        }

        if (currentMusicSource != null && currentMusicSource.clip.name == id)
        {
            Fade(currentMusicSource.clip, fadeDuration, volume);
            Debug.Log("Music already playing: " + id);
            return;
        }

        AudioClip clip = GetMusicClip(id);

        if (clip == null)
        {
            Debug.LogError("Music '" + id + "' is not exist");
            return;
        }

        if (currentMusicSource == null)
        {
            currentMusicSource = audioContainer.AddComponent<AudioSource>();
            currentMusicSource.clip = clip;
            currentMusicSource.mute = false;
            currentMusicSource.ignoreListenerPause = true;
            currentMusicSource.loop = true;
        }

        Fade(clip, fadeDuration, volume);
    }

    public void StopMusic(float fadeDuration = 1)
    {
        if (currentMusicSource == null)
        {
            return;
        }

        currentMusicSource.DOFade(0, fadeDuration).SetEase(Ease.Linear).OnComplete(() => currentMusicSource.Stop());
    }

    public void PauseMusic()
    {
        if (currentMusicSource == null)
        {
            return;
        }

        currentMusicSource.Pause();
    }

    public void ResumeMusic()
    {
        if (currentMusicSource == null)
        {
            return;
        }

        currentMusicSource.Play();
    }

    public void MusicVolume(float volume, float fadeDuration)
    {
        if (currentMusicSource == null)
        {
            return;
        }
        

        Fade(currentMusicSource.clip, fadeDuration, volume);
    }

    public void PlaySound(string id, float volume = 1, bool loop = false, int maxSameSounds = 2)
    {
        if (!isActiv)
            return;

        if (string.IsNullOrEmpty(id))
        {
            Debug.Log("Sound empty or null");
            return;
        }

        AudioClip clip = GetSoundClip(id);
        if (clip == null)
        {
            Debug.LogError("Sound '" + id + "' is not exist");
            return;
        }

        int countSameSounds = 0;
        foreach (AudioSource source in currentSoundSources)
        {
            if (source.clip.name == id)
            {
                countSameSounds++;
            }
        }

        if (countSameSounds >= maxSameSounds)
        {
            Debug.Log("Too much duplicates for sound: " + id);
            return;
        }

        AudioSource audioSource = audioContainer.AddComponent<AudioSource>();
        audioSource.clip = clip;
        audioSource.playOnAwake = false;
        audioSource.loop = loop;
        audioSource.mute = false;
        audioSource.volume = volume;
        audioSource.Play();
        currentSoundSources.Add(audioSource);

        if (!loop)
        {
            StartCoroutine(DelayStopSound(audioSource, audioSource.clip.length));
        }
    }

    public void StopSound(string id)
    {
        for (int i = 0; i < currentSoundSources.Count; i++)
        {
            if (currentSoundSources[i].clip.name == id)
            {
                Destroy(currentSoundSources[i]);
                currentSoundSources.Remove(currentSoundSources[i]);
                break;
            }
        }
    }

    public void PauseSound(string id)
    {
        ExistAudioSource(id)?.Pause();
    }

    public void ResumeSound(string id)
    {
        ExistAudioSource(id)?.Play();
    }

    private void Init()
    {
        CreateAudioContainer();
        LoadCachingSounds();
    }

    private void CreateAudioContainer()
    {
        audioContainer = new GameObject("AudioManagerContainer");
        audioContainer.transform.SetParent(transform);
    }

    void Fade(AudioClip clip, float fadeDuration, float volume)
    {
        if (currentMusicSource.clip != clip)
        {
            if (currentMusicSource.clip != null)
            {
                currentMusicSource.DOFade(0, fadeDuration).SetEase(Ease.Linear).SetUpdate(true);
            }
            else
            {
                currentMusicSource.volume = 0;
            }

            DOVirtual.DelayedCall(currentMusicSource.volume == 0 ? 0 : fadeDuration, delegate
            {
                currentMusicSource.clip = clip;
                if (!currentMusicSource.isPlaying)
                {
                    currentMusicSource.Play();
                }

                currentMusicSource.DOFade(volume, fadeDuration).SetEase(Ease.Linear).SetUpdate(true);
                ;
            });
        }
        else
        {
            if (!currentMusicSource.isPlaying)
            {
                currentMusicSource.Play();
            }

            currentMusicSource.DOFade(volume, fadeDuration).SetEase(Ease.Linear).SetUpdate(true);
            ;
        }
    }

    private void LoadCachingSounds()
    {
        AudioClip[] resourceAudioClips = Resources.LoadAll<AudioClip>(cachingFolderPath);
        foreach (AudioClip clip in resourceAudioClips)
        {
            cachingSounds.Add(clip.name, clip);
        }
    }

    private AudioClip GetSoundClip(string id)
    {
        if (cachingSounds.ContainsKey(id))
        {
            return cachingSounds[id];
        }

        return Resources.Load<AudioClip>(notCachingFolderPath + id);
    }

    private AudioClip GetMusicClip(string id)
    {
        return Resources.Load<AudioClip>(musicFolderPath + id);
    }

    AudioSource ExistAudioSource(string id)
    {
        for (int i = 0; i < currentSoundSources.Count; i++)
        {
            if (currentSoundSources[i].clip.name == id)
            {
                return currentSoundSources[i];
            }
        }

        return null;
    }

    IEnumerator DelayStopSound(AudioSource audioSource, float delayValue)
    {
        yield return new WaitForSeconds(delayValue);
        Destroy(audioSource);
        currentSoundSources.Remove(audioSource);
    }
}