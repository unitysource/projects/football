﻿using System.Collections;
using UnityEngine;

public class Ball : BallBase
{
    public override void Init()
    {
        base.Init();
        forceVector = new Vector3(0.1f, maxYForce, maxZForce);
    }

    protected override void Aim()
    {
        base.Aim();

        if (!isActivate)
            return;

        if (blockAim)
            return;

        if (!fullAimActive)
            TrajectoryRenderer.Instance.ShowTrajectory(aimPoint.position, forceVector, isShowGroundEffect);
        else
            TrajectoryRenderer.Instance.ShowTrajectory(aimPoint.position, forceVector, isShowGroundEffect);


        if (Input.GetMouseButton(0) && IsOneFingerOnScreen())
        {
            SetForce();
        }

        if (Input.GetMouseButtonUp(0))
        {
            EventManager.TriggerEvent(CustomEventType.EVENT_ON_AIM_MOUSE_UP,
                new Hashtable() {{"isFinalGame", fullAimActive}});
        }
    }

    void SetForce()
    {
        if (!fullAimActive)
        {
            float xForce = Time.deltaTime * Input.GetAxis("Mouse X") * sensitivity * 3;
            mouseInWorld = new Vector3(mouseInWorld.x -= xForce, maxYForce, maxZForce);
        }
        else
        {
            float xForce = Time.deltaTime * Input.GetAxis("Mouse X") * 7.5f;
            float yForce = (Time.deltaTime * Input.GetAxis("Mouse Y") * 7.5f) * -1;
            float zForce = (Time.deltaTime * Input.GetAxis("Mouse Y") * 7.5f) * -1;
            mouseInWorld = new Vector3(mouseInWorld.x -= xForce, forceVector.y -= yForce, forceVector.y -= zForce);
        }


        float y = fullAimActive ? Mathf.Clamp(mouseInWorld.y, 3, 15) : maxYForce;
        float x = Mathf.Clamp(mouseInWorld.x * -1, -20, 20);
        float z = fullAimActive ? Mathf.Clamp(mouseInWorld.y * 4, 3, 30) : maxZForce;
        forceVector = new Vector3(x, y, z);
        force = forceVector;
        aimPoint.rotation = Quaternion.LookRotation(forceVector);
    }

    public override void ShootAfterGate(Vector3 direction)
    {
        base.ShootAfterGate(direction);
        SetTriggerCollider(0, false);
        SetShootState(0.5f, true);
        rb.AddForce(direction, ForceMode.Force);
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);
        if (isShoot)
        {
            if (collision.transform.CompareTag("Enemy"))
            {
                EffectManager.Instance.Show(EffectManager.EffectName.KickCollision, collision.transform.position,
                    Quaternion.identity, 2);
                collision.transform.TryGetComponent<Enemy>(out Enemy enemy);
                if (enemy != null)
                    enemy.ForceUp();
            }
        }
    }
}