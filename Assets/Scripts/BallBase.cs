﻿using System.Collections;
using DG.Tweening;
using MoreMountains.NiceVibrations;
using TrailsFX;
using UnityEngine;

public class BallBase : MonoBehaviour
{
    [SerializeField] protected SphereCollider spherePhysicsCollider;
    [SerializeField] protected SphereCollider trigerCollider;
    [SerializeField] protected Transform aimPoint;
    [SerializeField] protected TrailEffect trail;
    [SerializeField] protected float maxYForce = 5;
    [SerializeField] protected float maxZForce = 40;
    [SerializeField] protected float sensitivity = 0.1f;
    [SerializeField] protected float radiusConnectWithBall = 2;
    [SerializeField] protected bool isShowGroundEffect = false;
    [SerializeField] protected int maxNumberKickBall = 2;
    [SerializeField] protected int maxNumberEnemyContact = 3;
    [SerializeField] protected int maxNumberEnemyContactInIdleStte = 1;
    [SerializeField] protected float maxDistanceForDetectCollision = 10;
    protected Rigidbody rb;
    protected Vector3 forceVector = Vector3.zero;
    public static Vector3 force;
    protected bool isActivate = false;
    public bool isShoot = false;
    protected bool fullAimActive;
    private int numberKickBall = 0;
    private int numberEnemyContact = 0;
    private int numberEnemyContactInIdleState = 0;
    private bool isEnemyOnHorizonte = false;
    public bool isCanDetectCollision = true;
    protected bool blockAim = false;
    private Vector3 savedPos = Vector3.zero;
    protected Vector3 mouseInWorld = Vector3.zero;

    public bool IsPlayerShoot { get; set; }

    private void OnEnable()
    {
        EventManager.AddListener(CustomEventType.EVENT_ON_ANIM_KICK_DONE, OnShoot);
        EventManager.AddListener(CustomEventType.EVENT_ON_WIN, OnBlockAim);
    }

    private void OnDisable()
    {
        EventManager.AddListener(CustomEventType.EVENT_ON_ANIM_KICK_DONE, OnShoot);
        EventManager.RemoveListener(CustomEventType.EVENT_ON_WIN, OnBlockAim);
    }

    void Awake()
    {
        Init();
        trigerCollider.radius = radiusConnectWithBall;
    }

    private void OnBlockAim(Hashtable arg0)
    {
        blockAim = true;
    }

    protected bool IsOneFingerOnScreen()
    {
        bool isOneFingerOnScreen = false;

#if UNITY_EDITOR
        isOneFingerOnScreen = true;
#endif
#if !UNITY_EDITOR
                isOneFingerOnScreen = Input.touchCount == 1;
#endif

        return isOneFingerOnScreen;
    }

    public void SetTriggerCollider(float delay, bool state)
    {
        DOVirtual.DelayedCall(delay, () => trigerCollider.enabled = state);
    }

    protected virtual void Update()
    {
        if (!isActivate)
            return;

        Aim();
    }

    private void FixedUpdate()
    {
        CheckDestroy();
        SetVelocity();
        CheckCanDetectCollision();
    }

    void CheckCanDetectCollision()
    {
        if (!isShoot)
            return;

        if (savedPos == Vector3.zero)
        {
            savedPos = transform.position;
        }

        if (Vector3.Distance(transform.position, savedPos) >
            maxDistanceForDetectCollision)
        {
            isCanDetectCollision = false;
        }
        else
            isCanDetectCollision = true;
    }

    void SetVelocity()
    {
        if (isShoot && numberEnemyContact < maxNumberEnemyContact && isEnemyOnHorizonte)
        {
            rb.velocity = (Vector3.Normalize(force) * 50);
        }
    }

    void CheckDestroy()
    {
        if (!isShoot)
            return;

        if (isShoot && rb.velocity.sqrMagnitude <= 0.1f && numberKickBall >= maxNumberKickBall)
            Destroy();
        if (transform.position.y < 0)
            Destroy();
        if (isShoot && numberEnemyContact >= maxNumberEnemyContact)
            ForceDestroy();
    }

    void Destroy()
    {
        if (!spherePhysicsCollider.enabled)
            return;
        float delayShowEffect = 2.5f;

        DOVirtual.DelayedCall(delayShowEffect, () =>
        {
            EffectManager.Instance.Show(EffectManager.EffectName.BallDestroy, transform.position, Quaternion.identity,
                3);
            Destroy(gameObject);
        });
    }

    public virtual void Init()
    {
        isActivate = false;
        isShoot = false;
        trail.cam = Camera.main;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        rb = GetComponent<Rigidbody>();
        rb.isKinematic = false;
        savedPos = Vector3.zero;
    }

    protected void Shoot()
    {
        if (isShoot || !isActivate)
            return;

        GameObject newBall = Instantiate(gameObject, aimPoint.position, Quaternion.identity);
        newBall.TryGetComponent<Rigidbody>(out Rigidbody ballRigidbody);
        ballRigidbody.isKinematic = false;
        ballRigidbody.AddForce(forceVector, ForceMode.VelocityChange);
        newBall.TryGetComponent<BallBase>(out BallBase ball);
        ball.SetShootState(0.1f, true);
        ball.SetTriggerCollider(1, true);
        ball.IsPlayerShoot = true;
        ball.savedPos = Vector3.zero;
        numberKickBall++;
        ball.IncrementNumberKick(numberKickBall);
        TrajectoryRenderer.Instance.HideForceTrajectory();
        Core.Instance.SetTimeScale(1);
        gameObject.SetActive(false);
        EventManager.TriggerEvent(CustomEventType.EVENT_ON_CONTINUE_RUN, null);
        EventManager.TriggerEvent(CustomEventType.EVENT_ON_TIMER_STATE, new Hashtable() {{"timerState", false}});
        EffectManager.Instance.Show(EffectManager.EffectName.CickBall, transform.position, Quaternion.identity, 2);
        VirtualCameras.Instance.CickBall();
        MMVibrationManager.Haptic(HapticTypes.Selection);
        isShoot = true;
        IsPlayerShoot = true;
        savedPos = Vector3.zero;
    }

    public void IncrementNumberKick(int numberKick)
    {
        numberKickBall = numberKick;
    }

    public void Activate()
    {
        isActivate = true;
        trigerCollider.enabled = false;
    }

    public void SetRigidbodyKinematicState(bool state)
    {
        rb.isKinematic = state;
    }

    public void SetFullAimn(bool state)
    {
        fullAimActive = state;
    }

    public void SetShootState(float delay, bool state)
    {
        DOVirtual.DelayedCall(delay, () => { isShoot = state; });
    }

    public virtual void ShootAfterGate(Vector3 direction)
    {
        isShoot = true;
    }

    protected virtual void Aim()
    {
        if (blockAim)
            return;
        RaycastCheck();
    }

    public void ForceDestroy()
    {
        EffectManager.Instance.Show(EffectManager.EffectName.BallDestroy, transform.position, Quaternion.identity,
            3);
        Destroy(gameObject, 0.1f);
    }

    public void RaycastCheck()
    {
        RaycastHit[] hits;
        hits = Physics.RaycastAll(transform.position, forceVector, 100.0F);


        for (int i = 0; i < hits.Length; i++)
        {
            RaycastHit hit = hits[i];
            if (hit.transform.CompareTag("Partner"))
            {
                hit.transform.TryGetComponent(out PartnerController partner);
                partner.ShowEffect();
            }
        }
    }

    protected virtual void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Enemy"))
        {
            AudioManager.Instance.PlaySound("hit_human_enemy", volume: 0.3f, maxSameSounds: 2);
            isEnemyOnHorizonte = true;
            if (!isShoot && numberEnemyContactInIdleState >= maxNumberEnemyContactInIdleStte)
            {
                ForceDestroy();
                return;
            }

            if (numberEnemyContact < maxNumberEnemyContact)
            {
                MMVibrationManager.Haptic(HapticTypes.Selection);
                collision.transform.TryGetComponent(out Enemy enemy);
                if (enemy != null)
                    enemy.Death();
            }

            if (isShoot)
                numberEnemyContact++;
            else
                numberEnemyContactInIdleState++;
        }

        if (!isCanDetectCollision)
            return;

        if (collision.transform.CompareTag("BallBasket"))
        {
            collision.transform.TryGetComponent(out BallBasket basket);
            basket.ReleaseBalls();
            if (isShoot)
                ForceDestroy();
            AudioManager.Instance.PlaySound("hit_target_and_basket", volume: 0.85f);
            MMVibrationManager.Haptic(HapticTypes.Selection);
        }

        if (collision.transform.CompareTag("Obstacle"))
        {
            collision.transform.TryGetComponent(out Obstacle obstacle);
            obstacle.Destroy();
            ForceDestroy();
            AudioManager.Instance.PlaySound("hit_target_and_basket", volume: 0.85f);
            MMVibrationManager.Haptic(HapticTypes.Selection);
        }
    }

    private void OnShoot(Hashtable arg0)
    {
        Shoot();
    }
}