﻿using System.Collections;
using UnityEngine;

public class BallBasket : MonoBehaviour
{
    [SerializeField] private GameObject ballTemplate = null;
    [SerializeField] private int numberBallSpawn = 5;
    [SerializeField] private float minForce = 100;
    [SerializeField] private float maxForce = 500;
    [SerializeField] private Explosion explosion;
    [SerializeField] private GameObject[] parts;

    private bool isReleased = false;

    public void ReleaseBalls()
    {
        if (isReleased)
            return;

        StartCoroutine(ReleaseWork());
    }

    IEnumerator ReleaseWork()
    {
        isReleased = true;
        DisableParts();

        yield return new WaitForSeconds(0.1f);

        explosion.StartExplosion();
        for (int i = 0; i < numberBallSpawn; i++)
        {
            Vector3 pos1 = new Vector3(transform.position.x, transform.position.y + 3, transform.position.z);
            GameObject ball = Instantiate(ballTemplate, pos1, Quaternion.identity);
            ball.TryGetComponent(out Rigidbody rigid);
            rigid.interpolation = RigidbodyInterpolation.Interpolate;
            rigid.collisionDetectionMode = CollisionDetectionMode.ContinuousDynamic;
            rigid.AddForce(VectorForce(), ForceMode.Impulse);
            yield return new WaitForEndOfFrame();
        }

        EffectManager.Instance.Show(EffectManager.EffectName.BasketBallRelease, transform.position, Quaternion.identity,
            2);
        Vector3 pos = new Vector3(transform.position.x, transform.position.y + 3, transform.position.z);
        EffectManager.Instance.ShowPopUpMoney(pos, 5);
        MoneyManager.Instance.AddLevelMoney(5);
        MoneyManager.Instance.AddGameMoney(5);
        EventManager.TriggerEvent(CustomEventType.EVENT_ON_ADD_MONEY, null);
        Destroy(gameObject, 0.2f);
    }

    void DisableParts()
    {
        for (int i = 0; i < parts.Length; i++)
        {
            parts[i].gameObject.SetActive(false);
        }
    }

    public Vector3 VectorForce()
    {
        float xForce = Random.Range(-2, 2);
        float yForce = 1;
        float zForce = Random.Range(-2, 2);

        return new Vector3(xForce, yForce, zForce) * Random.Range(minForce, maxForce);
    }
}