﻿using UnityEngine;

public class BallExplosion : BallBase, IExplosion
{
    [SerializeField] private Explosion explosion;

    public override void Init()
    {
        base.Init();
        forceVector = new Vector3(0.01f, 8, 8);
    }

    protected override void Aim()
    {
        base.Aim();
        TrajectoryRenderer.Instance.ShowTrajectory(aimPoint.position, forceVector, isShowGroundEffect);

        if (Input.GetMouseButton(0) && IsOneFingerOnScreen())
            SetForce();

        if (Input.GetMouseButtonUp(0))
            EventManager.TriggerEvent(CustomEventType.EVENT_ON_AIM_MOUSE_UP, null);
    }

    void SetForce()
    {
        mouseInWorld = new Vector3(mouseInWorld.x -= (Time.deltaTime * Input.GetAxis("Mouse X") * sensitivity),
            forceVector.y -= (Time.deltaTime * Input.GetAxis("Mouse Y") * sensitivity) * -1,
            forceVector.y -= (Time.deltaTime * Input.GetAxis("Mouse Y") * sensitivity) * -1);


        float y = Mathf.Clamp(mouseInWorld.y, 5, 10);
        float x = Mathf.Clamp(mouseInWorld.x * -1, -4, 4);
        float z = Mathf.Clamp(mouseInWorld.y, 2, 25);
        forceVector = new Vector3(x, y, z);
    }

    public override void ShootAfterGate(Vector3 direction)
    {
        base.ShootAfterGate(direction);
        SetShootState(0.5f, true);
        Vector3 forceVector = new Vector3(0, 0, 1);
        rb.AddForce(forceVector * UnityEngine.Random.Range(1000, 3500), ForceMode.Force);
    }

    protected void OnCollisionEnter(Collision collision)
    {
        base.OnCollisionEnter(collision);

        if (collision.transform.CompareTag("Enemy"))
        {
            collision.transform.TryGetComponent(out Enemy enemy);
            if (enemy != null)
                enemy.Death();
        }

        if (isShoot)
        {
            gameObject.layer = LayerMask.NameToLayer("Ball");
            GetComponent<MeshRenderer>().enabled = false;
            explosion.StartExplosion();
            AudioManager.Instance.PlaySound("RedBallBoom", maxSameSounds: 1);
            Destroy(gameObject, 0.2f);
            isShoot = false;
        }
    }
}