﻿using System;
using UnityEngine;

public class ColliderEventHandler : MonoBehaviour
{
    public Action<GameObject> OnTriggerWithBall;
    public Action<GameObject, GameObject> OnTriggerWithBallThis;

    private void OnTriggerEnter(Collider collider)
    {
        if (collider.transform.CompareTag("Ball"))
        {
            OnTriggerWithBall?.Invoke(collider.gameObject);
            OnTriggerWithBallThis?.Invoke(gameObject, collider.gameObject);
        }
    }

    private void OnTriggerStay(Collider collider)
    {
        if (collider.transform.CompareTag("Ball"))
        {
            OnTriggerWithBall?.Invoke(collider.gameObject);
            OnTriggerWithBallThis?.Invoke(gameObject, collider.gameObject);
        }
    }
}