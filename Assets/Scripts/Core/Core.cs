using UnityEngine;
using System.Collections.Generic;
using System;
using System.Collections;
using DG.Tweening;

public class Core : MonoBehaviour
{
    private static Core instance = null;

    public bool IsGameOnPause { get; set; }

    void Awake()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    void Start()
    {
        InitGame();
        AudioManager.Instance.PlayMusic("Background", 0, fadeDuration: 0);
        AudioManager.Instance.SetAudioActive(SaveManager.Instance.GetBool(SaveKey.SOUND_STATE, true));
    }

    void InitGame()
    {
        Application.targetFrameRate = 60;
    }

    private IEnumerator ExecuteActionWithDelayCoroutine(float delay, Action action)
    {
        yield return new WaitForSeconds(delay);
        action();
    }

    public void SetTimeScale(float value)
    {
        if (value == 0)
            IsGameOnPause = true;
        else
            IsGameOnPause = false;

        Time.timeScale = value;
    }

    public static Core Instance
    {
        get { return instance; }
    }

    public void ExecuteActionWithDelay(float delay, Action action)
    {
        StartCoroutine(ExecuteActionWithDelayCoroutine(delay, action));
    }
}