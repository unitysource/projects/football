﻿using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public class EffectManager : MonoBehaviourSingleton<EffectManager>
{
    private List<EffectName> effectNames;
    private List<GameObject> activeEffects;

    public enum EffectName
    {
        BallFall,
        BasketBallRelease,
        CickBall,
        MagicZone,
        Ring,
        KickCollision,
        FireworkRedCluster,
        FireworkYellow,
        BigExplosion,
        BallDestroy,
        Disappear,
        Confetti,
        PopUpMoney,
    }

    public GameObject[] effects;

    private void Awake()
    {
        effectNames = new List<EffectName>();
        activeEffects = new List<GameObject>();
    }

    public void Show(EffectName effectName, Vector3 position, Quaternion rotation, float destroyDelay,
        bool isMany = false)
    {
        if (effectNames.Contains(effectName) && !isMany)
            return;

        effectNames.Add(effectName);
        GameObject template = GetEffect(effectName);
        GameObject effect = Instantiate(template, position, rotation);
        activeEffects.Add(effect);
        Destroy(effect, destroyDelay);
        DOVirtual.DelayedCall(destroyDelay, () =>
        {
            effectNames.Remove(effectName);
            activeEffects.Remove(effect);
        });
    }

    public void ShowPopUpMoney(Vector3 position, int value, float destroyDelay = 2)
    {
        GameObject popUpMoney = Get(EffectName.PopUpMoney, position, Quaternion.identity);
        popUpMoney.TryGetComponent(out PopUpMoney popUp);
        popUp.SetText(value);
        Destroy(popUpMoney, destroyDelay);
    }

    public GameObject Get(EffectName effectName, Vector3 position, Quaternion rotation)
    {
        GameObject template = GetEffect(effectName);
        GameObject effect = Instantiate(template, position, rotation);

        return effect;
    }


    public void Hide(EffectName effectName)
    {
        for (int i = 0; i < activeEffects.Count; i++)
        {
            if (activeEffects[i] != null && activeEffects[i].name == effectName.ToString() + "(Clone)")
            {
                Destroy(activeEffects[i]);
                activeEffects.RemoveAt(i);
                effectNames.Remove(effectName);
            }
        }
    }

    GameObject GetEffect(EffectName effectName)
    {
        for (int i = 0; i < effects.Length; i++)
        {
            if (effects[i].transform.name == effectName.ToString())
            {
                return effects[i];
            }
        }

        return null;
    }
}