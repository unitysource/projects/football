﻿using System;
using DG.Tweening;
using UnityEngine;
using Random = UnityEngine.Random;

public class Enemy : MonoBehaviour
{
    [SerializeField] private Collider physicsCollider;
    [SerializeField] private Rigidbody[] ragdollsParts;
    private float speedMove = 5;
    private Animator animator;
    private State state = State.NONE;

    enum State
    {
        NONE,
        WALK,
        DEATH,
    }

    private void Awake()
    {
        Init();
    }

    void Start()
    {
        EnemysController.Instance.AddEnemy(this);
    }

    public bool IsDeath()
    {
        if (state == State.DEATH)
            return true;

        return false;
    }

    private void Update()
    {
        CheckDistanceAndDestroy();
    }

    void CheckDistanceAndDestroy()
    {
        if (transform.position.z < VirtualCameras.Instance.MainCamera.transform.position.z)
        {
            EnemysController.Instance.RemoveEnemy(this);
            Destroy(gameObject, 1);
        }
    }

    void Init()
    {
        animator = GetComponent<Animator>();
        SewtRagdollsState(true);
        state = State.WALK;
        transform.rotation = Quaternion.Euler(0, 0, 0);
        SetAnimation();
    }

    void SewtRagdollsState(bool isKinematic = true)
    {
        for (int i = 0; i < ragdollsParts.Length; i++)
        {
            ragdollsParts[i].isKinematic = isKinematic;
        }
    }

    public void SetAnimationSpeed(float speed)
    {
        animator.SetFloat("Speed", speed);
    }

    void Move()
    {
        if (state != State.WALK)
            return;

        transform.Translate(Vector3.forward * Time.deltaTime * speedMove);
    }

    public void ForceUp()
    {
        for (int i = 0; i < ragdollsParts.Length; i++)
        {
            ragdollsParts[i]
                .AddForce(new Vector3(UnityEngine.Random.Range(-40, 40), 50, UnityEngine.Random.Range(-40, 40)),
                    ForceMode.Impulse);
        }
    }

    void SetAnimation()
    {
        int animIndex = Random.Range(1, 9);
        float timeRecord = Random.Range(0f, 1f);
        animator.Play("Idle_" + animIndex, 0, timeRecord);
    }

    public void Death()
    {
        if (state == State.DEATH)
            return;

        Vector3 pos = new Vector3(transform.position.x, transform.position.y + 3, transform.position.z);
        EffectManager.Instance.ShowPopUpMoney(pos, 1);
        MoneyManager.Instance.AddLevelMoney(1);
        MoneyManager.Instance.AddGameMoney(1);
        EventManager.TriggerEvent(CustomEventType.EVENT_ON_ADD_MONEY, null);
        physicsCollider.enabled = false;
        transform.SetParent(null);
        Destroy(gameObject, 3);
        SewtRagdollsState(false);
        state = State.DEATH;
        animator.enabled = false;
        EnemysController.Instance.RemoveEnemy(this);

        DOVirtual.DelayedCall(0.2f, () =>
        {
            for (int i = 0; i < ragdollsParts.Length; i++)
            {
                ragdollsParts[i].gameObject.layer = LayerMask.NameToLayer("ExclusionForSimulation");
            }
        });
    }


    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
            Death();
    }

    private void OnCollisionStay(Collision collision)
    {
        if (collision.gameObject.CompareTag("Ball"))
            Death();
    }
}