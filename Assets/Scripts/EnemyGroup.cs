﻿using System.Collections;
using UnityEngine;

public class EnemyGroup : MonoBehaviour
{
    [SerializeField] float animationSpeed = 1;
    private Enemy[] enemyItem;

    private void OnEnable()
    {
        EventManager.AddListener(CustomEventType.EVENT_ON_GAMEPLAY_START, OnStartMove);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener(CustomEventType.EVENT_ON_GAMEPLAY_START, OnStartMove);
    }

    private void Start()
    {
        Init();
    }

    void OnStartMove(Hashtable parameters)
    {
        //path.DOPlay();
        SetAnimSpeed(animationSpeed);
    }

    void Init()
    {
        //path = GetComponent<DOTweenPath>();
        enemyItem = transform.GetComponentsInChildren<Enemy>();
        SetAnimSpeed(0);
    }

    void SetAnimSpeed(float speed)
    {
        foreach (var item in enemyItem)
        {
            item.SetAnimationSpeed(speed);
        }
    }
}