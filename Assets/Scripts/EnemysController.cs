﻿using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class EnemysController : MonoBehaviourSingleton<EnemysController>
{
    private List<Enemy> enemys;
    private int index = 0;

    private void Awake()
    {
        enemys = new List<Enemy>();
    }

    public void AddEnemy(Enemy enemy)
    {
        enemys.Add(enemy);
    }

    public void RemoveEnemy(Enemy enemy)
    {
        enemys.Remove(enemy);
    }

    public GameObject GetEnemy(Vector3 originPos)
    {
        GameObject enemy = null;
        enemys = enemys.OrderBy((d) => (d.transform.position - originPos).sqrMagnitude).ToList();
        if (index < enemys.Count)
            enemy = enemys[index].gameObject;

        index++;
        if (index >= enemys.Count - 1)
        {
            index = 0;
        }
        
        if(enemy.transform.position.z > originPos.z)
            return enemy;

        return null;
    }
}