﻿using UnityEngine;

public class Explosion : MonoBehaviour
{
    [SerializeField] private float radiusExplosionWave = 5;
    [SerializeField] private float forceExplosionWave = 100;
    [SerializeField] EffectManager.EffectName effectType = EffectManager.EffectName.BallFall;

    private SphereCollider collider;
    private Rigidbody rigidBody;

    private void Awake()
    {
        Init();
    }

    void Init()
    {
        collider = GetComponent<SphereCollider>();
        rigidBody = GetComponent<Rigidbody>();
        rigidBody.isKinematic = true;
        collider.enabled = false;
    }

    public void StartExplosion()
    {
        rigidBody.isKinematic = false;
        collider.enabled = true;
        Quaternion eularRotation = Quaternion.Euler(-90, 0, 0);
        ExplosionWave();
        Destroy(gameObject, 0.2f);
        Vector3 pos = transform.position;
        pos.y = 0.1f;
        EffectManager.Instance.Show(effectType, pos, eularRotation, 2);
    }

    void ExplosionWave()
    {
        collider.radius = radiusExplosionWave;
        Collider[] collisions = Physics.OverlapSphere(transform.position, radiusExplosionWave);

        foreach (var col in collisions)
        {
            col.gameObject.TryGetComponent(out Enemy enemy);
            col.gameObject.TryGetComponent(out Rigidbody rig);

            if (enemy != null)
                enemy.Death();

            if (rig != null && col.GetComponent<BallBase>() == null)
            {
                rig.AddExplosionForce(forceExplosionWave, transform.position, radiusExplosionWave, 50);
            }

            col.gameObject.TryGetComponent(out Obstacle obstacle);
            if (obstacle != null)
                obstacle.Destroy();

            col.gameObject.TryGetComponent(out BallBasket basket);
            if (basket != null)
                basket.ReleaseBalls();
        }
    }
}