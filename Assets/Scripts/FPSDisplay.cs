﻿using UnityEngine;

public class FPSDisplay : MonoBehaviour
{
    GUIStyle styleGUI = new GUIStyle();
    float deltaTime = 0.0f;

    void Start()
    {
        DontDestroyOnLoad(gameObject);
        styleGUI.fontSize = 40;
        styleGUI.normal.textColor = Color.blue;
    }

    void Update()
    {
        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;
    }

    void OnGUI()
    {
        float msec = deltaTime * 1000.0f;
        float fps = 1.0f / deltaTime;
        string text = string.Format("{0:0.0} ms ({1:0.} fps)", msec, fps);
        GUI.Label(new Rect(10, 10, 0, 0), text, styleGUI);
    }
}