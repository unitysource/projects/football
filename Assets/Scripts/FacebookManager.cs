﻿using Facebook.Unity;

public class FacebookManager : MonoBehaviourSingleton<FacebookManager>
{
    private void Awake()
    {
        Init();
    }
    
    void Init()
    {
        if (FB.IsInitialized)
            FB.ActivateApp();
        else
            FB.Init(() => { FB.ActivateApp(); });
    }
}