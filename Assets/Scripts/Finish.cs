﻿using System;
using System.Collections;
using DG.Tweening;
using UnityEngine;

public class Finish : MonoBehaviour
{
    [SerializeField] private float gateLeftBound = -1.9f;
    [SerializeField] private float gateRightBound = 1.9f;
    [SerializeField] private float gateMoveDuration = 1;
    [SerializeField] private Gate gate;
    [SerializeField] private GameObject ballTemplate;
    [SerializeField] private Transform ballSpawnPoint;
    [SerializeField] private float durationBallMove = 0.5f;
    [SerializeField] private int maxNumberKickBall = 3;
    [SerializeField] private GameObject longShootEffectPoint;
    [SerializeField] private GameObject[] fireworkPoints;

    private float delayShowWin = 1;
    private int moneyMultiplier = 1;
    private bool isActiv = false;
    private Tween delayTween = null;
    private int counterKickBall = 0;

    private void OnEnable()
    {
        EventManager.AddListener(CustomEventType.EVENT_ON_START_FINISH_GAME, OnStartFinishGame);
        EventManager.AddListener(CustomEventType.EVENT_ON_ANIM_KICK_DONE, OnNextBall);
        moneyMultiplier = 1;
    }

    private void OnDisable()
    {
        EventManager.AddListener(CustomEventType.EVENT_ON_START_FINISH_GAME, OnStartFinishGame);
        EventManager.AddListener(CustomEventType.EVENT_ON_ANIM_KICK_DONE, OnNextBall);
        if (delayTween != null)
            delayTween.Kill();
    }

    void Awake()
    {
        gate.OnBallContact += OnLongShot;
        gate.OnTargetDestroy += OnHitCounting;
        gate.transform.position = new Vector3(gateLeftBound, gate.transform.position.y, gate.transform.position.z);
        moneyMultiplier = 1;
    }

    private void Start()
    {
        moneyMultiplier = 1;
    }

    void OnLongShot()
    {
        if (isActiv)
            return;
        AudioManager.Instance.PlaySound("footballCrowd", 0.8f);
    }

    void OnHitCounting()
    {
        moneyMultiplier++;
        MoneyManager.Instance.SetMultiplierMoney(moneyMultiplier);
    }

    void GateMove()
    {
        gate.transform.DOMoveX(gateRightBound, gateMoveDuration).SetEase(Ease.InOutQuad).SetLoops(-1, LoopType.Yoyo);
    }

    private void OnStartFinishGame(Hashtable arg0)
    {
        ShowFirework(4);
        isActiv = true;
        GateMove();
        gate.ShowRandomAimSector();
        SpawnBall();
    }

    void ShowFirework(int value)
    {
        for (int i = 0; i < value; i++)
        {
            int rndIndexPoint = UnityEngine.Random.Range(0, fireworkPoints.Length);
            EffectManager.Instance.Show(EffectManager.EffectName.Confetti,
                fireworkPoints[rndIndexPoint].transform.position,
                Quaternion.identity, 3, true);
        }
    }

    void SpawnBall()
    {
        if (!isActiv)
            return;

        GameObject ball = Instantiate(ballTemplate, ballSpawnPoint.position, Quaternion.identity);
        ball.transform
            .DOMove(
                new Vector3(SwichPlayer.CurrentPlayer.transform.position.x, ball.transform.position.y + 0.5f,
                    SwichPlayer.CurrentPlayer.transform.position.z + 1), durationBallMove).OnComplete(() =>
            {
                ball.TryGetComponent(out BallBase ballBase);
                ballBase.Init();
                ballBase.SetFullAimn(true);
                ballBase.Activate();
                ballBase.SetRigidbodyKinematicState(true);
            }).SetDelay(0.5f);
    }

    private void OnNextBall(Hashtable arg0)
    {
        if (!isActiv)
            return;

        SpawnBall();
        counterKickBall++;
        EventManager.TriggerEvent(CustomEventType.EVENT_ON_FINISH_GAME_KICK_BALL, null);
        if (counterKickBall > maxNumberKickBall - 1)
        {
            ShowFirework(4);
            AudioManager.Instance.PlaySound("leve_ done", volume: 0.9f);
            AudioManager.Instance.PlaySound("footballCrowd", 0.8f);
            delayTween = DOVirtual.DelayedCall(delayShowWin, () =>
            {
                EventManager.TriggerEvent(CustomEventType.EVENT_ON_WIN, null);
                //Core.Instance.SetTimeScale(0);
            });
        }
    }
}