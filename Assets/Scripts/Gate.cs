﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Gate : MonoBehaviour
{
    [SerializeField] private GameObject[] gateAimSector;
    [SerializeField] private Transform[] aimSectrosPoint;

    private List<int> savedId;
    private int aimIndex = 0;
    public Action OnBallContact;
    public Action OnTargetDestroy;

    private void Awake()
    {
        savedId = new List<int>();
        InitAllAimSectors();
        HideAllAimSectors();
    }

    public void ShowRandomAimSector()
    {
        // int indexPoint = RandomData.Instance.GetRandomTarget(0, aimSectrosPoint.Length);
        // for (int i = 0; i < aimSectrosPoint.Length; i++)
        // {
        //     if (i == indexPoint)
        //         gateAimSector[i].gameObject.SetActive(true);
        //     else
        //         gateAimSector[i].gameObject.SetActive(false);
        // }

        int randomPoints = RandomData.Instance.GetRandomTarget(0, aimSectrosPoint.Length);

        for (int i = 0; i < gateAimSector.Length; i++)
        {
            if (i == aimIndex)
            {
                gateAimSector[i].gameObject.SetActive(true);
                gateAimSector[i].transform.position = aimSectrosPoint[randomPoints].transform.position;
            }
            else
            {
                gateAimSector[i].gameObject.SetActive(false);
            }
        }

        aimIndex++;
        if (aimIndex > gateAimSector.Length)
            aimIndex = 0;
    }

    void HideAllAimSectors()
    {
        for (int i = 0; i < gateAimSector.Length; i++)
        {
            gateAimSector[i].gameObject.SetActive(false);
        }
    }

    void InitAllAimSectors()
    {
        for (int i = 0; i < gateAimSector.Length; i++)
        {
            gateAimSector[i].GetComponent<ColliderEventHandler>().OnTriggerWithBallThis += OnDestroyTarget;
        }
    }

    void OnDestroyTarget(GameObject obj, GameObject collider)
    {
        if (!savedId.Contains(collider.GetInstanceID()))
        {
            savedId.Add(collider.GetInstanceID());
            obj.GetComponent<MeshDestroy>().DestroyMesh();
            ShowRandomAimSector();
            AudioManager.Instance.PlaySound("hit_target_and_basket", 0.85f);
            collider.TryGetComponent(out BallBase ball);
            if (ball != null && ball.IsPlayerShoot)
                OnTargetDestroy?.Invoke();
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.transform.CompareTag("Ball"))
            ReactionOnBall(collision);
    }

    void ReactionOnBall(Collision collision)
    {
        OnBallContact?.Invoke();
        collision.transform.TryGetComponent(out BallBase ballBase);
        if (ballBase != null && ballBase.IsPlayerShoot)
            ballBase.ForceDestroy();
    }
}