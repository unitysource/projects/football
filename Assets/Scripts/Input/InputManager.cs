﻿using System;
using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections.Generic;
using System.Collections;

public class InputManager : MonoBehaviourSingleton<InputManager>
{
    [SerializeField] private float sensitivitySwipe = 0.5f;
    private Vector2 prevResolution = Vector2.zero;
    private Vector3 prevMousePos = Vector3.zero;
    private Vector3 mousePosDelta = Vector3.zero;
    private Vector3 mousePosDeltaSmoothed = Vector3.zero;
    private float mouseScaleDelta = 0.0f;
    private int maxMousePosDeltaSmoothListSize = 3;
    private int prevTouchCount = 0;
    private bool isMouseUpHappened = false;
    private bool isMouseDownHappened = false;
    private bool isMouseDragHappened = false;
    private bool isMouseHappened = false;
    private bool isChangeDeviceOrientaionHappened = false;
    private bool isMouseOverUI = false;
    private List<Vector3> mousePosDeltaSmoothList = new List<Vector3>();
    private Vector2 firstPressPos;
    private Vector2 secondPressPos;
    private Vector2 currentSwipe;

    public Action OnRightSwipe;
    public Action OnLeftSwipe;
    public Action OnUpSwipe;
    public Action OnDownSwipe;

    void Update()
    {
        mousePosDelta = prevMousePos - Input.mousePosition;
        mouseScaleDelta = Input.GetAxis("Mouse ScrollWheel");
        isMouseUpHappened = Input.GetMouseButtonUp(0) || (prevTouchCount > Input.touchCount);
        isMouseDownHappened = Input.GetMouseButtonDown(0);
        isMouseHappened = Input.GetMouseButton(0);
        isMouseDragHappened = Input.GetMouseButton(0) && IsMouseMoveHappened() && (Input.touchCount < 2);
        isChangeDeviceOrientaionHappened = (Screen.width != prevResolution.x) || (Screen.height != prevResolution.y);
        isMouseOverUI = IsPointerOverUIObject();

        prevResolution.x = Screen.width;
        prevResolution.y = Screen.height;
        prevMousePos = Input.mousePosition;
        prevTouchCount = Input.touchCount;
        Swipe();

        if (Input.touchCount == 2)
        {
            Touch touchZero = Input.GetTouch(0);
            Touch touchOne = Input.GetTouch(1);
            Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
            Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;
            float prevTouchDistance = (touchZeroPrevPos - touchOnePrevPos).magnitude;
            float touchDistance = (touchZero.position - touchOne.position).magnitude;
            float touchDelta = touchDistance - prevTouchDistance;
            mouseScaleDelta = touchDelta / 160.0f;
        }

        if (isMouseDownHappened)
        {
            mousePosDeltaSmoothList.Clear();
        }

        if (IsMouseDragHappened())
        {
            if (mousePosDeltaSmoothList.Count > maxMousePosDeltaSmoothListSize)
                mousePosDeltaSmoothList.RemoveAt(0);

            mousePosDeltaSmoothList.Add(mousePosDelta);
        }
    }

    void Swipe()
    {
        if (Input.GetMouseButtonDown(0))
        {
            firstPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
        }

        if (Input.GetMouseButtonUp(0))
        {
            secondPressPos = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
            currentSwipe = new Vector2(secondPressPos.x - firstPressPos.x, secondPressPos.y - firstPressPos.y);
            currentSwipe.Normalize();

            if (currentSwipe.y > 0 && currentSwipe.x > -sensitivitySwipe && currentSwipe.x < sensitivitySwipe)
            {
                OnUpSwipe?.Invoke();
            }

            if (currentSwipe.y < 0 && currentSwipe.x > -sensitivitySwipe && currentSwipe.x < sensitivitySwipe)
            {
                OnDownSwipe?.Invoke();
            }

            if (currentSwipe.x < 0 && currentSwipe.y > -sensitivitySwipe && currentSwipe.y < sensitivitySwipe)
            {
                OnLeftSwipe?.Invoke();
            }

            if (currentSwipe.x > 0 && currentSwipe.y > -sensitivitySwipe && currentSwipe.y < sensitivitySwipe)
            {
                OnRightSwipe?.Invoke();
            }
        }
    }

    public Vector3 MouseMoveDelta()
    {
        return mousePosDelta;
    }

    public Vector3 SmoothMouseMoveDelta()
    {
        if (mousePosDeltaSmoothList.Count == 0)
            return Vector3.zero;

        mousePosDeltaSmoothed = Vector3.zero;
        mousePosDeltaSmoothList.ForEach((Vector3 posDelta) => { mousePosDeltaSmoothed += posDelta; });
        mousePosDeltaSmoothed /= mousePosDeltaSmoothList.Count;

        return mousePosDeltaSmoothed;
    }

    public float MouseScaleDelta()
    {
        return mouseScaleDelta;
    }

    public Vector3 GetMousePosInWorldPoint(Transform obj)
    {
        Vector3 mouse = MousePos;
        Vector3 mouseWorld = Camera.main.ScreenToWorldPoint(new Vector3(mouse.x, mouse.y, obj.position.y));
        Vector3 forward = mouseWorld - obj.position;
        return forward;
    }


    public bool IsMouseMoveHappened()
    {
        return !isMouseDownHappened && !isMouseUpHappened && (mousePosDelta != Vector3.zero);
    }

    public bool IsMouseScaleHappened()
    {
        return mouseScaleDelta != 0.0f;
    }

    public bool IsMouseUpHappened()
    {
        return isMouseUpHappened;
    }

    public bool IsMouseDownHappened()
    {
        return isMouseDownHappened;
    }

    public bool IsMouseHappened()
    {
        return isMouseHappened;
    }

    public bool IsMouseDragHappened()
    {
        return isMouseDragHappened;
    }

    public bool IsChangeDeviceOrientaionHappened()
    {
        return isChangeDeviceOrientaionHappened;
    }

    public bool IsMouseOverUI()
    {
        if (prevMousePos != Input.mousePosition)
        {
            isMouseOverUI = IsPointerOverUIObject();
        }

        return isMouseOverUI;
    }

    public bool IsUserInputEnabled()
    {
        return Core.Instance.GetComponent<EventSystem>().enabled;
    }

    public Vector3 MousePos
    {
        get { return Input.mousePosition; }
    }

    private bool IsPointerOverUIObject()
    {
#if !UNITY_EDITOR
			if(!EventSystem.current)
			{
				return true;
			}

			PointerEventData eventDataCurrentPosition = new PointerEventData(EventSystem.current);
			eventDataCurrentPosition.position = new Vector2(Input.mousePosition.x, Input.mousePosition.y);
			List<RaycastResult> results = new List<RaycastResult>();
			EventSystem.current.RaycastAll(eventDataCurrentPosition, results);
			return results.Count > 0;
#endif
        return false;
    }
}