using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelLogic : MonoBehaviour
{
    [SerializeField] private Transform spawnPlayerPosition;
    private GameObject playerObject;
    private float timer = 0;
    private float progress = 0;
    private Vector3 finishPos;

    private void Start()
    {
        SpawnPlayer();
        Core.Instance.SetTimeScale(1);
        EventManager.TriggerEvent(CustomEventType.EVENT_ON_TIMER_STATE, new Hashtable() {{"timerState", false}});
        timer = 0;
        SetFinishPos();
        MoneyManager.Instance.SetMultiplierMoney(1);
        
        
        VirtualCameras.Instance.SetRunVirtualCamera(playerObject.transform, playerObject.transform);
        MoneyManager.Instance.ResetMoney();

        AppMetrica.Instance.ReportEvent("level_start", new Dictionary<string, object>
        {
            {"level_number", LevelManager.Instance.CurrentLevelIndex}
        });
        AppMetrica.Instance.SendEventsBuffer();
    }

    void CheckDistance()
    {
        float globalDistance = Vector3.Distance(spawnPlayerPosition.position, finishPos);
        float playerdistanceToEnd = Vector3.Distance(SwichPlayer.CurrentPlayer.transform.position, finishPos);
        float playerDistance = ((playerdistanceToEnd / 100) / globalDistance) * 100;
        progress = 1 - playerDistance;
    }

    void SetFinishPos()
    {
        finishPos = GameObject.Find("Finish").transform.position;
    }

    private void OnEnable()
    {
        EventManager.AddListener(CustomEventType.EVENT_ON_WIN, OnWin);
        EventManager.AddListener(CustomEventType.EVENT_ON_LOSE, OnLose);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener(CustomEventType.EVENT_ON_WIN, OnWin);
        EventManager.AddListener(CustomEventType.EVENT_ON_LOSE, OnLose);
    }

    private void OnLose(Hashtable arg0)
    {
        AppMetricaEvent(false);
    }

    private void OnWin(Hashtable arg0)
    {
        AppMetricaEvent(true);
    }

    void AppMetricaEvent(bool isWin)
    {
        AppMetrica.Instance.ReportEvent("level_finish", new Dictionary<string, object>
        {
            {"level_number", LevelManager.Instance.CurrentLevelIndex},
            {"result", isWin ? "win" : "lose"},
            {"time", GetLevelTime()},
            {"progress", (isWin ? 1 : GetLevelProgress())}
        });
        AppMetrica.Instance.SendEventsBuffer();
    }

    int GetLevelTime()
    {
        return (int) timer;
    }

    float GetLevelProgress()
    {
        return progress;
    }


    void Update()
    {
        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        if (Input.GetKeyDown(KeyCode.N))
        {
            LevelManager.Instance.NextLevel();
        }

        timer += Time.deltaTime;
        CheckDistance();
    }

    void SpawnPlayer()
    {
        GameObject playerObjectTmp = Resources.Load<GameObject>("Prefabs/Players/Player");
        playerObject = Instantiate(playerObjectTmp, spawnPlayerPosition.position, Quaternion.identity);
        playerObject.TryGetComponent(out PlayerController player);
        SwichPlayer.CurrentPlayer = player;
    }
}