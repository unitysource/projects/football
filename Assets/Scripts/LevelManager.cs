﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviourSingleton<LevelManager>
{
    public int CurrentLevelIndex { get; set; }
    private int sceneNumbers = 0;

    [SerializeField] private int sceneIndex = -1;

    void Start()
    {
        Init();
    }

    void Init()
    {
        sceneNumbers = SceneManager.sceneCountInBuildSettings - 1;
        CurrentLevelIndex = SaveManager.Instance.GetInt(SaveKey.LEVEL_NUMBER, 1);
        if (CheckAvailableScene())
            SceneManager.LoadScene("Level_" + CurrentLevelIndex);
        //SceneManager.LoadScene("Level_" + sceneIndex);
    }

    public void NextLevel()
    {
        int randomIndex = Random.Range(1, sceneNumbers);
        CurrentLevelIndex += 1;
        int sceneIndex = CurrentLevelIndex > sceneNumbers ? randomIndex : CurrentLevelIndex;
        SaveManager.Instance.SetInt(SaveKey.LEVEL_NUMBER, CurrentLevelIndex);
        SceneManager.LoadScene("Level_" + sceneIndex);
        //SceneManager.LoadScene("Level_" + sceneIndex);
    }

    bool CheckAvailableScene()
    {
        if (sceneNumbers >= CurrentLevelIndex)
            return true;

        return false;
    }
}