﻿public class MoneyManager : MonoBehaviourSingleton<MoneyManager>
{
    private int levelMoney;
    private int gameMoney;
    private int multiplierMoney = 1;

    private void Start()
    {
        Init();
    }

    void Init()
    {
        gameMoney = SaveManager.Instance.GetInt(SaveKey.GAME_MONEY_VALUE, 0);
    }

    public void ResetMoney()
    {
        gameMoney = SaveManager.Instance.GetInt(SaveKey.GAME_MONEY_VALUE, 0);
        levelMoney = 0;
    }

    public void AddLevelMoney(int value)
    {
        levelMoney += value;
    }

    public int GetCurrentLevelMoney()
    {
        return levelMoney;
    }

    public void AddGameMoney(int value)
    {
        gameMoney += value;
        SaveManager.Instance.SetInt(SaveKey.GAME_MONEY_VALUE, gameMoney);
    }

    public int GetGameMoney()
    {
        gameMoney = SaveManager.Instance.GetInt(SaveKey.GAME_MONEY_VALUE, 0);
        return gameMoney;
    }

    public void SetMultiplierMoney(int multiplier)
    {
        multiplierMoney = multiplier;
    }

    public int GetMultiplierMoney()
    {
        return multiplierMoney;
    }

    // public void StartFlyMoney(float duration, Vector3 firstPos, Vector3 lastPos)
    // {
    //     Vector3 midlPos = Utils.GetCurvePoint(firstPos, lastPos, 10);
    //     Vector3[] path = new[] {firstPos, midlPos, lastPos};
    //     Quaternion rotation = Quaternion.Euler(90, 0, 0);
    //     GameObject money = Instantiate(
    //         EffectManager.Instance.Get(EffectManager.EffectName.MoneyFly, firstPos, rotation),
    //         firstPos, rotation);
    //
    //     //money.transform.DOPath(path, 0.3f, PathType.CatmullRom, PathMode.Full3D);
    //     lastPos.y += 2;
    //     //lastPos.x += 10;
    //     
    //     money.transform.DOMove(lastPos, 1f);
    //     money.transform.DOScale(new Vector3(0, 0, 0), 1f);
    // }
}