﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class MultiplierBallGate : MonoBehaviour
{
    [SerializeField] private ColliderEventHandler leftSideMultiplierCollider;
    [SerializeField] private ColliderEventHandler rightSideMultiplierCollider;
    [SerializeField] private TextMeshPro textLeftSide;
    [SerializeField] private TextMeshPro textRightSide;
    [SerializeField] private int leftSideMultiplierValue = 4;
    [SerializeField] private int rightSideMultiplierValue = 8;

    private List<GameObject> balls;
    private bool isCanSpawn = true;

    private void Awake()
    {
        balls = new List<GameObject>();
        leftSideMultiplierCollider.OnTriggerWithBall += MultiplierBallLeftSide;
        rightSideMultiplierCollider.OnTriggerWithBall += MultiplierBallRightSide;
        textLeftSide.text = "x" + leftSideMultiplierValue;
        if (textRightSide != null)
            textRightSide.text = "x" + rightSideMultiplierValue;
    }

    private void MultiplierBallRightSide(GameObject ball)
    {
        MultiplierBall(rightSideMultiplierValue, ball);
    }

    private void MultiplierBallLeftSide(GameObject ball)
    {
        MultiplierBall(leftSideMultiplierValue, ball);
    }

    void MultiplierBall(int value, GameObject ball)
    {
        ball.TryGetComponent(out BallBase ballBase);

        if (!isCanSpawn)
            return;

        if (ballBase != null && !ballBase.isCanDetectCollision)
            return;

        if (ball.transform.position.z <= SwichPlayer.CurrentPlayer.transform.position.z)
            return;

        
        if (ballBase.IsPlayerShoot)
        {
            Hide();
        }

        isCanSpawn = false;
        StartCoroutine(ShootBalls(value, ball));
    }

    IEnumerator ShootBalls(int value, GameObject ball)
    {
        for (int i = 0; i < value; i++)
        {
            if (ball != null)
            {
                GameObject ballTmp = Instantiate(ball, ball.transform.position, Quaternion.identity);
                ballTmp.layer = LayerMask.NameToLayer("ExclusionForSimulation");
                GameObject enemy = EnemysController.Instance.GetEnemy(ball.transform.position);
                if (enemy != null)
                {
                    balls.Add(ballTmp);
                    Vector3 target = enemy.transform.position;
                    Vector3 dir = target - ball.transform.position;
                    dir = dir.normalized;
                    float rndForce = UnityEngine.Random.Range(4000, 5500);
                    //Debug.DrawRay(ball.transform.position, dir * rndForce, Color.red, Mathf.Infinity);

                    ballTmp.TryGetComponent(out BallBase ballBase);
                    ballBase.ShootAfterGate(dir * rndForce);
                }
                else
                {
                    ballTmp.TryGetComponent(out BallBase ballBase);
                    ballBase.ShootAfterGate(new Vector3(UnityEngine.Random.Range(-0.5f, 0.5f),
                        0, 1) * 3000);
                }
            }

            //yield return new WaitForEndOfFrame();
        }


        yield return new WaitForSeconds(1);
        isCanSpawn = true;
        for (int i = 0; i < balls.Count; i++)
        {
            if (balls[i] != null)
            {
                balls[i].layer = LayerMask.NameToLayer("Ball");
            }
        }
    }

    void Hide()
    {
        AudioManager.Instance.PlaySound("ballGateDone", 0.9f);
        transform.DOScaleY(0, 0.5f).OnComplete(() => Destroy(gameObject));
    }
}