﻿using UnityEngine;

public class Obstacle : MonoBehaviour
{
    private MeshDestroy meshDestroyer;

    private void Awake()
    {
        meshDestroyer = GetComponent<MeshDestroy>();
    }

    public void Destroy()
    {
        AudioManager.Instance.PlaySound("hit_target_and_basket", 0.85f);
        Vector3 pos = new Vector3(transform.position.x, transform.position.y + 2, transform.position.z);
        EffectManager.Instance.ShowPopUpMoney(pos, 1);
        MoneyManager.Instance.AddLevelMoney(1);
        MoneyManager.Instance.AddGameMoney(1);
        EventManager.TriggerEvent(CustomEventType.EVENT_ON_ADD_MONEY, null);
        meshDestroyer.DestroyMesh();
    }
    
}