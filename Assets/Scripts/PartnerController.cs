﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

public class PartnerController : MonoBehaviour
{
    private PlayerController player;
    [SerializeField] private Transform ballPoint;
    [SerializeField] private float durationBallMove = 1;
    [SerializeField] private Explosion explosion;
    [SerializeField] private float minDistanceToPlayer = 5;
    private bool isReceivingBall = false;
    private Animator animator;
    private bool isFlipWork = false;

    void Awake()
    {
        animator = GetComponent<Animator>();
        animator.Play("Waving");
        player = GetComponent<PlayerController>();
        player.enabled = false;
    }

    private void OnCollisionEnter(Collision collider)
    {
        if (!isReceivingBall && !PlayerController.IsAiming)
        {
            // if (collider.transform.CompareTag("Ball") && TrajectoryRenderer.Instance.IsPhysicsPathSimulate)
            if (collider.transform.CompareTag("Ball"))
            {
                collider.transform.TryGetComponent(out BallBase balBase);
                if (balBase != null && !(balBase is IExplosion) && balBase.IsPlayerShoot)
                {
                    isReceivingBall = true;
                    ReceivingBall(collider);
                }
            }
        }

        if (collider.transform.CompareTag("Enemy"))
        {
            //    player.Death();
            animator.Play("Fall");
            Destroy(gameObject, 2);
        }
    }

    public void ShowEffect()
    {
        EffectManager.Instance.Show(EffectManager.EffectName.MagicZone, transform.position, Quaternion.identity, 2);
    }

    void CheckDistance()
    {
        if (SwichPlayer.CurrentPlayer.transform.position.z > transform.position.z - minDistanceToPlayer &&
            !isReceivingBall)
        {
            if (!isFlipWork)
                StartCoroutine(FlipWork());

            isFlipWork = true;
        }
    }

    // private void OnTriggerEnter(Collider collider)
    // {
    //     if (collider.transform.CompareTag("Player"))
    //     {
    //         StartCoroutine(FlipWork());
    //     }
    // }

    void Update()
    {
        CheckDistance();
    }

    IEnumerator FlipWork()
    {
        float rotationDuration = 0.2f;
        float moveDelay = 0.15f;
        animator.Play("Rotate");
        transform.DORotate(new Vector3(0, 90, 0), rotationDuration);

        yield return new WaitForSeconds(rotationDuration);
        animator.Play("Flip");

        yield return new WaitForSeconds(moveDelay);
        transform.DOMoveX(transform.position.x - 5, 1f);
    }

    public void Destroy()
    {
        EffectManager.Instance.Show(EffectManager.EffectName.Disappear, transform.position, Quaternion.identity, 3);
        Destroy(gameObject);
    }

    void ReceivingBall(Collision collider)
    {
        TrajectoryRenderer.Instance.HideForceTrajectory();
        SwichPlayer.CurrentPlayer.Deactivate();

        SwichPlayer.CurrentPlayer = player;
        player.enabled = true;
        player.ReactWithBall(collider.gameObject, ballPoint.gameObject);
        AudioManager.Instance.PlaySound("PartnerWave", maxSameSounds: 1);
        if (explosion != null)
            explosion.StartExplosion();

        collider.gameObject.TryGetComponent(out Rigidbody rigidbody);
        rigidbody.velocity = Vector3.zero;
        rigidbody.isKinematic = true;

        collider.gameObject.TryGetComponent(out BallBase ballBase);
        ballBase.transform.DOMove(ballPoint.position, durationBallMove).OnComplete(() =>
        {
            ballBase.Init();
            ballBase.Activate();
        });
       
        //Destroy(GetComponent<PartnerController>());
        EffectManager.Instance.Hide(EffectManager.EffectName.MagicZone);
    }
}