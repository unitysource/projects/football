﻿using System.Collections;
using DG.Tweening;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] private float speedMove = 5;
    [SerializeField] private float leftBound = -4;
    [SerializeField] private float rightBound = 4;
    [SerializeField] private float sensitivitySideMove = 20;

    private Animator animator = null;
    private State state = State.NONE;
    private float currentAimTimer;
    private bool aimTimerWork = false;
    private Tween moveTween = null;
    private Tween delayTween;

    public static bool IsAiming { get; set; }

    private enum State
    {
        NONE,
        IDLE,
        RUN,
        AIM,
        WIN,
        LOSE,
        DEACTIVATE,
        DEATH,
        FINISH_GAME,
        AUTOMOVE,
    }

    private void OnEnable()
    {
        EventManager.AddListener(CustomEventType.EVENT_ON_CONTINUE_RUN, OnContinueRun);
        EventManager.AddListener(CustomEventType.EVENT_ON_AIM_MOUSE_UP, OnFastCick);
        if (delayTween != null)
            delayTween.Kill();
    }

    private void OnDisable()
    {
        EventManager.RemoveListener(CustomEventType.EVENT_ON_CONTINUE_RUN, OnContinueRun);
        EventManager.RemoveListener(CustomEventType.EVENT_ON_AIM_MOUSE_UP, OnFastCick);
        if (delayTween != null)
            delayTween.Kill();
    }

    private void Awake()
    {
        animator = GetComponent<Animator>();
        state = State.IDLE;
    }

    void Update()
    {
        CheckGameplayStart();
        Move();
        TimerAim();
    }

    void Move()
    {
        if (state == State.RUN)
        {
            transform.position += Vector3.forward * Time.deltaTime * speedMove;

            if (InputManager.Instance.IsMouseDragHappened())
            {
                float posX = Input.GetAxis("Mouse X") * Time.deltaTime * sensitivitySideMove;
                transform.position += new Vector3(posX, 0, 0);
                transform.position = new Vector3(Mathf.Clamp(transform.position.x, leftBound, rightBound),
                    transform.position.y, transform.position.z);
            }
        }
    }

    bool CheckState(State state)
    {
        if (this.state == state)
            return true;
        else
            return false;
    }

    void SetState(State state)
    {
        this.state = state;
    }

    void CheckGameplayStart()
    {
        if (InputManager.Instance.IsMouseDownHappened() && CheckState(State.IDLE))
        {
            EventManager.TriggerEvent(CustomEventType.EVENT_ON_GAMEPLAY_START, null);
            SetState(State.RUN);
            animator.SetBool("IsRun", true);
        }
    }

    private void OnTriggerEnter(Collider collider)
    {
        TriggerEvent(collider);
    }

    private void OnTriggerStay(Collider collider)
    {
        TriggerEvent(collider);
    }

    void TriggerEvent(Collider collider)
    {
        
        if (collider.transform.CompareTag("FinishLine"))
            ReactWithFinishLine();
        
        if (SwichPlayer.CurrentPlayer != this)
            return;
        
        if (!gameObject.activeInHierarchy)
            return;
        
        // if (transform.position.z > collider.transform.position.z - 0.5f)
        //     return;
        if (collider.CompareTag("Ball"))
            ReactWithBall(collider.gameObject);
        if (collider.transform.CompareTag("Enemy"))
            ReactWithEnemy(collider.gameObject);
        if (collider.transform.CompareTag("Obstacle"))
        {
            collider.transform.TryGetComponent(out Obstacle obstacle);
            obstacle.Destroy();
        }

        if (collider.CompareTag("BallBasket"))
        {
            collider.TryGetComponent(out BallBasket basket);
            basket.ReleaseBalls();
        }
    }

    void ReactWithFinishLine()
    {
        if (state == State.FINISH_GAME || state == State.AUTOMOVE)
            return;

        SetState(State.AUTOMOVE);
        transform.DOMove(new Vector3(0, transform.position.y, transform.position.z + 1), 0.4f).OnComplete(() =>
        {
            SetState(State.FINISH_GAME);
            EventManager.TriggerEvent(CustomEventType.EVENT_ON_START_FINISH_GAME, null);
            VirtualCameras.Instance.SetFinishVirtualCamera(transform, transform);
            animator.SetBool("IsRun", false);
            animator.Play("IdleFinishLevel");
        });
    }

    public void ReactWithBall(GameObject collider, GameObject pointAim = null)
    {
        if (CheckState(State.AIM) || CheckState(State.FINISH_GAME))
            return;

        collider.TryGetComponent(out BallBase ball);

        //BallsController.Instance.ChangeLayer(collider.gameObject);
        IsAiming = true;
        Vector3 positionToMove = collider.transform.position;
        if (pointAim != null)
            positionToMove = pointAim.transform.position;
        VirtualCameras.Instance.SetAimVirtualCamera(transform, transform);
        DOVirtual.DelayedCall(0.5f, () => Core.Instance.SetTimeScale(0.5f));
        SetState(State.AIM);
        // animator.SetBool("IsWalk", true);
        animator.SetFloat("Speed", 1.15f);
        animator.Play("Idle");
        animator.Play("KickBall");

        ball.Init();
        ball.SetRigidbodyKinematicState(true);
        //DOVirtual.DelayedCall(1, () => ball.Activate());
        ball.Activate();
        aimTimerWork = true;
        currentAimTimer = GameConst.durationAimTime;
        EventManager.TriggerEvent(CustomEventType.EVENT_ON_TIMER_STATE, new Hashtable() {{"timerState", true}});
        moveTween = transform.DOMove(new Vector3(positionToMove.x, 0, positionToMove.z - 0.7f),
            GameConst.durationAimTime).SetEase(Ease.Linear);
    }

    public void OnKickDone()
    {
        animator.SetFloat("Speed", 2);
        // Core.Instance.SetTimeScale(1);
        EventManager.TriggerEvent(CustomEventType.EVENT_ON_ANIM_KICK_DONE, null);
        IsAiming = false;
        //BallsController.Instance.ChangeLayerBack();
    }

    public void OnContinueRun()
    {
        if (state == State.DEACTIVATE)
            return;

        animator.SetFloat("Speed", 2);
        animator.Play("Run");
        VirtualCameras.Instance.SetRunVirtualCamera(transform, transform);
        SetState(State.RUN);
        animator.SetBool("IsRun", true);

        if (moveTween != null)
            moveTween.Kill(false);
    }

    private void OnFastCick(Hashtable arg0)
    {
        animator.SetFloat("Speed", 2);
        Core.Instance.SetTimeScale(1);

        if (arg0 != null && arg0["isFinalGame"] != null)
        {
            bool isFinalGame = (bool) arg0["isFinalGame"];
            if (isFinalGame)
            {
                animator.SetBool("IsKickBall", true);
                DOVirtual.DelayedCall(0.5f, () => animator.SetBool("IsKickBall", false));
            }
        }
    }

    void ReactWithEnemy(GameObject enemyObject)
    {
        enemyObject.transform.TryGetComponent(out Enemy enemy);
        if (!enemy.IsDeath())
        {
            SetState(State.LOSE);
            EventManager.TriggerEvent(CustomEventType.EVENT_ON_LOSE, null);
            animator.SetBool("IsFall", true);
            //delayTween = DOVirtual.DelayedCall(2, () => );
            Core.Instance.SetTimeScale(0);
        }
    }

    public void PlayKickSound()
    {
        AudioManager.Instance.PlaySound("kick_ball");
    }

    public void Death()
    {
        SetState(State.DEATH);
        animator.Play("Fall");
    }

    private void OnContinueRun(Hashtable arg0)
    {
        // VirtualCameras.Instance.SetRunVirtualCamera(transform, transform);
        // SetState(State.RUN);
        // animator.SetBool("IsRun", true);
    }

    public void Deactivate()
    {
        SetState(State.DEACTIVATE);
        Destroy(gameObject, 1);
    }


    void TimerAim()
    {
        if (!aimTimerWork)
            return;

        currentAimTimer -= Time.deltaTime;
        EventManager.TriggerEvent(CustomEventType.EVENT_ON_TIMER_AIM, new Hashtable() {{"timer", currentAimTimer}});
        if (currentAimTimer <= 0)
        {
            aimTimerWork = false;
            currentAimTimer = GameConst.durationAimTime;
            EventManager.TriggerEvent(CustomEventType.EVENT_ON_TIMER_STATE, new Hashtable() {{"timerState", false}});
            //EventManager.TriggerEvent(EventType.EVENT_ON_AUTO_SHOOT_BALL, null);
        }
    }
}