﻿using TMPro;
using UnityEngine;

public class PopUpMoney : MonoBehaviour
{
    [SerializeField] private TextMeshPro textValue;

    public void SetText(int valueMoney)
    {
        textValue.text = "+" + valueMoney + "<sprite name=\"coin_star\">";
    }
}