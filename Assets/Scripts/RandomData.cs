﻿public class RandomData : MonoBehaviourSingleton<RandomData>
{
    private static int targetRandomNumber;

    public int GetRandomTarget(int min, int max)
    {
        int result = UnityEngine.Random.Range(min, max);

        if (result == targetRandomNumber)
            return GetRandomTarget(min, max);

        targetRandomNumber = result;
        return result;
    }
    
}