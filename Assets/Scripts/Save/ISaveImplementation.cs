﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface ISaveImplementation
{
    void Save();
    void Load();
    void DeleteSave();
    void ClearHashTable();
    bool HasKey(string key);
    void SetValue(string key, string value);
    int GetInt(string key);
    long GetInt64(string key);
    string GetString(string key);
    bool GetBool(string key);
    double GetDouble(string key);
    float GetFloat(string key);
}