﻿using UnityEngine;
using System;

public enum SaveKey
{
	NONE,
	LEVEL_NUMBER,
	SOUND_STATE,
	VIBRATION_STATE,
	GAME_MONEY_VALUE,
};

public class SaveManager : MonoBehaviourSingleton<SaveManager>
{
	private ISaveImplementation saver;

	void Awake()
	{
		saver = new XmlSaveImplementation();
		saver.Load();
	}

	public void OnApplicationQuit()
	{
		saver.Save();
	}


	void OnApplicationPause(bool paused)
	{
		if (paused)
		{
			saver.Save();
		}
	}

	public void SetInt(SaveKey key, int value)
	{
		saver.SetValue(key.ToString(), value.ToString());
	}

	public int GetInt(SaveKey key, int defaultValue = 0)
	{
		if (saver.HasKey(key.ToString()))
		{
			return saver.GetInt(key.ToString());
		}
		return defaultValue;
	}

	public void SetInt64(SaveKey key, long value)
	{
		saver.SetValue(key.ToString(), value.ToString());
	}

	public long GetInt64(SaveKey key, int defaultValue = 0)
	{
		if (saver.HasKey(key.ToString()))
		{
			return saver.GetInt64(key.ToString());
		}
		return defaultValue;
	}

	public void SetFloat(SaveKey key, float value)
	{
		saver.SetValue(key.ToString(), value.ToString());
	}

	public float GetFloat(SaveKey key, float defaultValue = 0.0f)
	{
		if (saver.HasKey(key.ToString()))
		{
			return saver.GetFloat(key.ToString());
		}
		return defaultValue;
	}

	public void SetDouble(SaveKey key, double value)
	{
		saver.SetValue(key.ToString(), value.ToString());
	}

	public double GetDouble(SaveKey key, double defaultValue = 0.0f)
	{
		if (saver.HasKey(key.ToString()))
		{
			return saver.GetDouble(key.ToString());
		}
		return defaultValue;
	}

	public void SetString(SaveKey key, string value)
	{
		saver.SetValue(key.ToString(), value.ToString());
	}

	public string GetString(SaveKey key, string defaultValue = "")
	{
		if (saver.HasKey(key.ToString()))
		{
			return saver.GetString(key.ToString());
		}
		return defaultValue;
	}

	public void SetBool(SaveKey key, bool value)
	{
		saver.SetValue(key.ToString(), value.ToString());
	}

	public bool GetBool(SaveKey key, bool defaultValue = false)
	{
		if (saver.HasKey(key.ToString()))
		{
			return saver.GetBool(key.ToString());
		}
		return defaultValue;
	}

	public void Save()
	{
		saver.Save();
	}

	public void Load()
	{
		saver.Load();
	}

	public void DeleteSave()
	{
		saver.DeleteSave();
	}

	public void ClearHashTable()
	{
		saver.ClearHashTable();
	}
}
