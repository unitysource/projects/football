﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO;
using System.Xml;
using System;
using System.Security.Cryptography;
using System.Security.AccessControl;
using System.Text;

public class XmlSaveImplementation : ISaveImplementation
{
    private string encryptionKey = "MAKV2SPBNI99212";
    private string saveFileName = "GameSettingsSave.xml";
    private string saveFilePath;
    Hashtable saveTable = new Hashtable();

    public XmlSaveImplementation()
    {
        saveFilePath = Path.Combine(Application.persistentDataPath, saveFileName);
    }

    public void Save()
    {
        XmlDocument xmlDoc = new XmlDocument();
        XmlNode rootNode = xmlDoc.CreateElement("SaveData");
        xmlDoc.AppendChild(rootNode);

        foreach (DictionaryEntry item in saveTable)
        {
            XmlNode saveNode = xmlDoc.CreateElement(item.Key.ToString());
            saveNode.InnerText = item.Value.ToString();
            rootNode.AppendChild(saveNode);
        }

        xmlDoc.Save(saveFilePath);
    }

    public void Load()
    {
        if (!File.Exists(saveFilePath))
        {
            return;
        }

        XmlDocument xmlDoc = new XmlDocument();
        xmlDoc.Load(saveFilePath);

        foreach (XmlNode node in xmlDoc.DocumentElement.ChildNodes)
        {
            saveTable[node.Name] = node.InnerText;
        }
    }

    public void SetValue(string key, string value)
    {
        saveTable[key] = value;
    }

    public int GetInt(string key)
    {
        return Convert.ToInt32(saveTable[key]);
    }

    public long GetInt64(string key)
    {
        return Convert.ToInt64(saveTable[key]);
    }

    public string GetString(string key)
    {
        return saveTable[key].ToString();
    }

    public bool GetBool(string key)
    {
        return Convert.ToBoolean(saveTable[key]);
    }

    public double GetDouble(string key)
    {
        return Convert.ToDouble(saveTable[key]);
    }

    public float GetFloat(string key)
    {
        return Convert.ToSingle(saveTable[key]);
    }

    public void DeleteSave()
    {
        File.Delete(saveFilePath);
    }

    public void ClearHashTable()
    {
        saveTable.Clear();
    }

    public bool HasKey(string key)
    {
        return saveTable.ContainsKey(key);
    }

    private void Encrypt(string path, MemoryStream xmlStream)
    {
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(encryptionKey,
                new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (FileStream fsOutput = new FileStream(path, FileMode.Create))
            {
                using (CryptoStream cs = new CryptoStream(fsOutput, encryptor.CreateEncryptor(),
                    CryptoStreamMode.Write))
                {
                    int data;
                    while ((data = xmlStream.ReadByte()) != -1)
                    {
                        cs.WriteByte((byte) data);
                    }
                }
            }
        }
    }

    private void Decrypt(string path, MemoryStream xmlStream)
    {
        using (Aes encryptor = Aes.Create())
        {
            Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(encryptionKey,
                new byte[] {0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76});
            encryptor.Key = pdb.GetBytes(32);
            encryptor.IV = pdb.GetBytes(16);
            using (FileStream fsInput = new FileStream(path, FileMode.Open))
            {
                using (CryptoStream cs = new CryptoStream(fsInput, encryptor.CreateDecryptor(), CryptoStreamMode.Read))
                {
                    int data;
                    while ((data = cs.ReadByte()) != -1)
                    {
                        xmlStream.WriteByte((byte) data);
                    }

                    xmlStream.Flush();
                    xmlStream.Position = 0;
                }
            }
        }
    }
}