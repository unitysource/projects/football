﻿using System.Collections;
using UnityEngine;

public class TrajectoryRenderer : MonoBehaviourSingleton<TrajectoryRenderer>
{
    private LineRenderer lineRenderer;
    [SerializeField] private GameObject aimObject;
    [SerializeField] private Transform[] aims;

    private int indexLastActiveObjectInTrajectory = 0;

    private GameObject effect;

    private void Awake()
    {
        lineRenderer = GetComponent<LineRenderer>();
    }

    public void ShowTrajectory(Vector3 origin, Vector3 speed, bool isShowGroundEffect = false)
    {
        Vector3[] points = new Vector3[100];

        for (int i = 0; i < points.Length; i++)
        {
            float time = i * 0.05f;
            points[i] = origin + speed * time + Physics.gravity * time * time / 2;
            aims[i].gameObject.SetActive(true);
            aims[i].position = points[i];

            if (aims[i].gameObject.activeInHierarchy && aims[i].position.y > 0)
            {
                indexLastActiveObjectInTrajectory = i;
            }

            if (aims[i].position.y < 0)
            {
                if (isShowGroundEffect)
                {
                    Quaternion rotation = Quaternion.Euler(90, 0, 0);
                    Vector3 pos = aims[indexLastActiveObjectInTrajectory].transform.position;
                    pos.y = 0.008f;
                    if (effect != null)
                        effect.transform.localPosition = pos;
                    else
                        effect = EffectManager.Instance.Get(EffectManager.EffectName.Ring, pos, rotation);
                }


                aims[i].gameObject.SetActive(false);
            }
        }
    }

    public void ShowTrajectoryPhysics(GameObject ball, Vector3 origin, Vector3 speed)
    {
        GameObject ballTmp = Instantiate(ball, origin, Quaternion.identity);
        ballTmp.GetComponent<Rigidbody>().AddForce(speed, ForceMode.VelocityChange);
        Physics.autoSimulation = false;
        Vector3[] points = new Vector3[100];
        //lineRenderer.positionCount = points.Length;

        for (int i = 0; i < points.Length; i++)
        {
            Physics.Simulate(Time.fixedDeltaTime);
            points[i] = ballTmp.transform.position;

            if (i % 6 == 0)
            {
                aims[i].gameObject.SetActive(true);
                aims[i].position = points[i];
                if (i > 0)
                {
                    Vector3 size = new Vector3(aims[i].transform.localScale.x - i / 100,
                        aims[i].transform.localScale.y - i / 100, aims[i].transform.localScale.z - i / 100);
                    aims[i].transform.localScale = size;
                }
            }
            else
            {
                aims[i].gameObject.SetActive(false);
            }


            // lineRenderer.SetPosition(i, points[i]);
        }

        Physics.autoSimulation = true;
        Destroy(ballTmp);
    }

    public void HideTrajectory(float duration)
    {
        StartCoroutine(HideAims(duration));
    }

    public void HideForceTrajectory()
    {
        Destroy(effect);
        for (int i = 0; i < aims.Length; i++)
        {
            aims[i].gameObject.SetActive(false);
        }
    }

    IEnumerator HideAims(float duration)
    {
        float delayTime = duration / aims.Length;
        for (int i = 0; i < aims.Length; i++)
        {
            yield return new WaitForSeconds(delayTime);
            aims[i].gameObject.SetActive(false);
        }
    }
}