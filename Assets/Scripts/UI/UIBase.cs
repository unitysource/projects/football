using DG.Tweening;
using UnityEngine;

public class UIBase : MonoBehaviour
{
    [SerializeField] protected float durationAnim = 0.5f;
    [SerializeField] protected Ease ease = Ease.OutElastic;
}