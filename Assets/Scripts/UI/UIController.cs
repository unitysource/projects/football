﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;

public class UIController : MonoBehaviour
{
    [SerializeField] private GameObject firstGameplayScreen;
    [SerializeField] private GameObject gameplayScreen;
    [SerializeField] private GameObject loseScreen;
    [SerializeField] private GameObject winScreen;
    [SerializeField] private GameObject settingsScreen;
    [SerializeField] private GameObject finishScreen;
    [SerializeField] private TextMeshProUGUI timerText;
    [SerializeField] private RectTransform niceReсt;

    private void OnEnable()
    {
        EventManager.AddListener(CustomEventType.EVENT_ON_GAMEPLAY_START, OnGameplayScreenShow);
        EventManager.AddListener(CustomEventType.EVENT_ON_TIMER_AIM, OnTimer);
        EventManager.AddListener(CustomEventType.EVENT_ON_TIMER_STATE, OnTimerState);
        EventManager.AddListener(CustomEventType.EVENT_ON_LOSE, OnLoseShow);
        EventManager.AddListener(CustomEventType.EVENT_ON_LONG_SHOT, OnShowNice);
        EventManager.AddListener(CustomEventType.EVENT_ON_WIN, OnWinShow);
        EventManager.AddListener(CustomEventType.EVENT_ON_START_FINISH_GAME, OnFinishShow);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener(CustomEventType.EVENT_ON_GAMEPLAY_START, OnGameplayScreenShow);
        EventManager.RemoveListener(CustomEventType.EVENT_ON_TIMER_AIM, OnTimer);
        EventManager.RemoveListener(CustomEventType.EVENT_ON_TIMER_STATE, OnTimerState);
        EventManager.RemoveListener(CustomEventType.EVENT_ON_LOSE, OnLoseShow);
        EventManager.RemoveListener(CustomEventType.EVENT_ON_LONG_SHOT, OnShowNice);
        EventManager.RemoveListener(CustomEventType.EVENT_ON_WIN, OnWinShow);
        EventManager.RemoveListener(CustomEventType.EVENT_ON_START_FINISH_GAME, OnFinishShow);
    }

    private void Awake()
    {
        firstGameplayScreen.SetActive(true);
        loseScreen.SetActive(false);
        winScreen.SetActive(false);
        loseScreen.SetActive(false);
        settingsScreen.SetActive(false);
        gameplayScreen.SetActive(false);
        finishScreen.SetActive(false);
        timerText.gameObject.SetActive(false);
        niceReсt.DOAnchorPosX(1000, 0);

        gameplayScreen.TryGetComponent(out UIGameplay gameplayUI);
        gameplayUI.OnClickButton += OnSettingsShow;

        settingsScreen.TryGetComponent(out UISettings settingsUI);
        settingsUI.OnClickButtonBack += OnSettingsHide;
    }

    private void OnGameplayScreenShow(Hashtable arg0)
    {
        firstGameplayScreen.SetActive(false);
        gameplayScreen.SetActive(true);
    }

    void OnShowNice(Hashtable parameters)
    {
        // niceReсt.DOAnchorPosX(0, 1).SetEase(Ease.OutElastic).OnComplete(() =>
        //     niceReсt.DOAnchorPosX(-1000, 1).SetEase(Ease.InElastic));
    }

    private void OnTimerState(Hashtable parameters)
    {
        bool timerState = (bool) parameters["timerState"];
        timerText.gameObject.SetActive(timerState);
    }

    private void OnTimer(Hashtable parameters)
    {
        float timer = (float) parameters["timer"];
        timerText.text = timer.ToString("0.000");
    }

    private void OnLoseShow(Hashtable parameters)
    {
        loseScreen.SetActive(true);
        gameplayScreen.SetActive(false);
    }


    private void OnWinShow(Hashtable arg0)
    {
        gameplayScreen.SetActive(false);
        winScreen.SetActive(true);
        finishScreen.SetActive(false);
    }

    void OnSettingsShow()
    {
        settingsScreen.SetActive(true);
        gameplayScreen.SetActive(false);
    }

    void OnSettingsHide()
    {
        settingsScreen.SetActive(false);
        gameplayScreen.SetActive(true);
    }

    private void OnFinishShow(Hashtable arg0)
    {
        finishScreen.SetActive(true);
    }
}