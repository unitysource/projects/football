﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIFinish : MonoBehaviour
{
    [SerializeField] private RectTransform[] ballsKick;
    private int indexBall;

    private void OnEnable()
    {
        EventManager.AddListener(CustomEventType.EVENT_ON_FINISH_GAME_KICK_BALL, OnDecrementBall);
    }

    private void OnDisable()
    {
        EventManager.RemoveListener(CustomEventType.EVENT_ON_FINISH_GAME_KICK_BALL, OnDecrementBall);
    }

    private void Awake()
    {
        indexBall = 0;
    }

    private void OnDecrementBall(Hashtable arg0)
    {
        ballsKick[indexBall].gameObject.SetActive(false);
        indexBall++;
    }
}