﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIGameplay : UIBase
{
    [SerializeField] private Button settingsButton;
    [SerializeField] private TextMeshProUGUI levelNumberText;
    [SerializeField] private TextMeshProUGUI moneyLevelText;
    [SerializeField] private GameObject templateCoin;

    public Action OnClickButton;

    private void OnEnable()
    {
        EventManager.AddListener(CustomEventType.EVENT_ON_ADD_MONEY, OnChnageMoney);
        ShowInterface();
    }

    private void OnDisable()
    {
        EventManager.RemoveListener(CustomEventType.EVENT_ON_ADD_MONEY, OnChnageMoney);
    }

    private void Awake()
    {
        settingsButton.onClick.AddListener(OnSettingsButtonClick);
    }

    private void Start()
    {
        Init();
    }

    void Init()
    {
        levelNumberText.gameObject.SetActive(true);
        levelNumberText.text = "LEVEL " + LevelManager.Instance.CurrentLevelIndex;
        moneyLevelText.text = MoneyManager.Instance.GetGameMoney().ToString();
    }

    void OnSettingsButtonClick()
    {
        AudioManager.Instance.PlaySound("button");
        OnClickButton?.Invoke();
        Core.Instance.SetTimeScale(0);
    }

    void ShowInterface()
    {
        RectTransform levelNumberRect = levelNumberText.GetComponent<RectTransform>();
        RectTransform settingsButton = this.settingsButton.GetComponent<RectTransform>();

        levelNumberRect.DOAnchorPosX(-1000, 0);
        settingsButton.DOScale(new Vector3(0, 0, 0), 0);
        levelNumberRect.DOAnchorPosX(0, durationAnim).SetEase(ease);
        settingsButton.DOScale(new Vector3(1, 1, 1), durationAnim).SetEase(ease);
    }

    private void OnChnageMoney(Hashtable parameters)
    {
        moneyLevelText.text = MoneyManager.Instance.GetGameMoney().ToString();
    }
}