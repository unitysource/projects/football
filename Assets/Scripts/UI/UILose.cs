﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UILose : UIBase
{
    [SerializeField] private RectTransform restartButtonRect;
    [SerializeField] private RectTransform loseRect;
    [SerializeField] private RectTransform levelNumberRect;
    [SerializeField] private TextMeshProUGUI levelNumberText;


    private void OnEnable()
    {
        restartButtonRect.GetComponent<Button>().onClick.AddListener(OnRestartButton);
        ShowInterface();
        levelNumberText.text = "LEVEL " + LevelManager.Instance.CurrentLevelIndex;
    }

    private void OnRestartButton()
    {
        AudioManager.Instance.PlaySound("button");
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    void ShowInterface()
    {
        loseRect.DOAnchorPosX(-1000, 0).SetUpdate(true);
        levelNumberRect.DOAnchorPosX(1000, 0).SetUpdate(true);
        restartButtonRect.DOAnchorPosX(1000, 0).SetUpdate(true);
        loseRect.DOAnchorPosX(0, durationAnim).SetEase(ease).SetUpdate(true);
        levelNumberRect.DOAnchorPosX(0, durationAnim).SetEase(ease).SetUpdate(true);
        restartButtonRect.DOAnchorPosX(0, durationAnim).SetEase(ease).SetUpdate(true);
    }
}