﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using MoreMountains.NiceVibrations;

public class UISettings : UIBase
{
    [SerializeField] private Button backButton;
    [SerializeField] private Button closeButton;
    [SerializeField] private Button soundButton;
    [SerializeField] private Image soundOnImage;
    [SerializeField] private Image soundOffImage;
    [SerializeField] private Button vibrationButton;
    [SerializeField] private Image vibrationOnImage;
    [SerializeField] private Image vibrationOffImage;
    [SerializeField] private RectTransform settingsWindow;

    private bool soundState;
    private bool vibrationState;
    public Action OnClickButtonBack;

    void Awake()
    {
        soundButton.onClick.AddListener(OnClickSoundButton);
        vibrationButton.onClick.AddListener(OnClickVibrationButton);
        backButton.onClick.AddListener(OnButtonBackClick);
        closeButton.onClick.AddListener(OnButtonBackClick);
    }

    private void OnEnable()
    {
        ShowInterface();
    }

    void ShowInterface()
    {
        settingsWindow.transform.DOScale(0, 0).SetUpdate(true);
        settingsWindow.transform.DOScale(1, durationAnim).SetEase(Ease.OutElastic).SetUpdate(true);
    }

    private void Start()
    {
        soundState = SaveManager.Instance.GetBool(SaveKey.SOUND_STATE, true);
        vibrationState = SaveManager.Instance.GetBool(SaveKey.VIBRATION_STATE, true);
        SetSoundState();
        SetVibrationState();
    }

    void OnClickSoundButton()
    {
        AudioManager.Instance.PlaySound("button");
        ChangeSoundState();
    }

    void SetSoundState()
    {
        if (soundState)
        {
            soundOnImage.enabled = true;
            soundOffImage.enabled = false;
        }
        else
        {
            soundOnImage.enabled = false;
            soundOffImage.enabled = true;
        }
    }

    void ChangeSoundState()
    {
        if (soundState)
        {
            soundOnImage.enabled = false;
            soundOffImage.enabled = true;
            soundState = false;
            SaveManager.Instance.SetBool(SaveKey.SOUND_STATE, soundState);
        }
        else
        {
            soundOnImage.enabled = true;
            soundOffImage.enabled = false;
            soundState = true;
            SaveManager.Instance.SetBool(SaveKey.SOUND_STATE, soundState);
        }

        AudioManager.Instance.SetAudioActive(soundState);
    }

    void SetVibrationState()
    {
        if (vibrationState)
        {
            vibrationOnImage.enabled = true;
            vibrationOffImage.enabled = false;
        }
        else
        {
            vibrationOnImage.enabled = false;
            vibrationOffImage.enabled = true;
        }
    }

    void ChangeVibrationState()
    {
        if (vibrationState)
        {
            vibrationOnImage.enabled = false;
            vibrationOffImage.enabled = true;
            vibrationState = false;
            SaveManager.Instance.SetBool(SaveKey.VIBRATION_STATE, vibrationState);
        }
        else
        {
            vibrationOnImage.enabled = true;
            vibrationOffImage.enabled = false;
            vibrationState = true;
            SaveManager.Instance.SetBool(SaveKey.VIBRATION_STATE, vibrationState);
        }

        MMVibrationManager.SetHapticsActive(vibrationState);
    }

    void OnClickVibrationButton()
    {
        AudioManager.Instance.PlaySound("button");
        ChangeVibrationState();
    }

    public void OnButtonBackClick()
    {
        AudioManager.Instance.PlaySound("button");
        Core.Instance.SetTimeScale(1);
        OnClickButtonBack?.Invoke();
    }
}