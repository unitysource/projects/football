﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class UIWin : UIBase
{
    [SerializeField] private Button nextButton;
    [SerializeField] private RectTransform winRect;
    [SerializeField] private RectTransform levelNumberRect;
    [SerializeField] private TextMeshProUGUI levelNumberText;
    [SerializeField] private TextMeshProUGUI gameMoneyText;
    [SerializeField] private TextMeshProUGUI levelMoneyText;
    [SerializeField] private TextMeshProUGUI multiplierMoneyText;
    [SerializeField] private RectTransform multiplierRect;
    [SerializeField] private RectTransform coinPosRect;
    [SerializeField] private RectTransform globalMoneyPosRect;
    [SerializeField] private GameObject coinTemplate;
    private bool isAnimCountingWork = true;


    private void OnEnable()
    {
        nextButton.onClick.AddListener(OnNextButton);
        ShowInterface();
      
    }

    private void Start()
    {
        SetMoneyInfo();
        StartAnimCountingMoney();
    }

    private void OnNextButton()
    {
        AudioManager.Instance.PlaySound("button");
        if (!isAnimCountingWork)
            LevelManager.Instance.NextLevel();
    }

    void SetMoneyInfo()
    {
        levelNumberText.text = "LEVEL " + LevelManager.Instance.CurrentLevelIndex;
        gameMoneyText.text = MoneyManager.Instance.GetGameMoney().ToString();
        levelMoneyText.text = "+" + MoneyManager.Instance.GetCurrentLevelMoney();
        multiplierMoneyText.text = "X" + MoneyManager.Instance.GetMultiplierMoney();
    }

    void ShowInterface()
    {
        RectTransform nextButtonRect = nextButton.GetComponent<RectTransform>();
        nextButtonRect.DOAnchorPosX(-1000, 0).SetUpdate(true);
        ;
        levelNumberRect.DOAnchorPosX(1000, 0).SetUpdate(true);
        ;
        winRect.DOAnchorPosX(-1000, 0).SetUpdate(true);
        ;

        nextButtonRect.DOAnchorPosX(0, durationAnim).SetEase(ease).SetUpdate(true);
        ;
        levelNumberRect.DOAnchorPosX(0, durationAnim).SetEase(ease).SetUpdate(true);
        ;
        winRect.DOAnchorPosX(0, durationAnim).SetEase(ease).SetUpdate(true);
        ;
    }

    void StartAnimCountingMoney()
    {
        StartCoroutine(AnimCountingMoney());
    }

    IEnumerator AnimCountingMoney()
    {
        multiplierRect.gameObject.SetActive(false);
        yield return new WaitForSeconds(0.1f);
        if (MoneyManager.Instance.GetMultiplierMoney() <= 1)
            multiplierRect.gameObject.SetActive(false);
        else
            multiplierRect.gameObject.SetActive(true);
        int moneyAdd = (MoneyManager.Instance.GetCurrentLevelMoney() * MoneyManager.Instance.GetMultiplierMoney()) -
                       MoneyManager.Instance.GetCurrentLevelMoney();

        int value = 0;
        float duration = 0.5f;
        yield return new WaitForSeconds(1);

        if (MoneyManager.Instance.GetMultiplierMoney() > 1)
            multiplierRect.transform.DOScale(Vector3.zero, 0.5f).SetEase(Ease.InElastic);

        if (MoneyManager.Instance.GetMultiplierMoney() > 1)
        {
            int startMoneyValue = MoneyManager.Instance.GetCurrentLevelMoney();
            int endMoneyValue = (startMoneyValue * MoneyManager.Instance.GetMultiplierMoney()) - startMoneyValue;
            float percent = 0;
            float timeFactor = 1 / duration;
            while (percent < 1)
            {
                percent += Time.deltaTime * timeFactor;
                value = (int) Mathf.Lerp(startMoneyValue, endMoneyValue, Mathf.SmoothStep(0, 1, percent));
                levelMoneyText.text = "+" + value;
                yield return null;
            }

            yield return new WaitForSeconds(duration);


            //int maxCoin = 10;
            int currentCount = 10;
            while (currentCount > 0)
            {
                GameObject coinTmp = Instantiate(coinPosRect.gameObject, coinPosRect.position, Quaternion.identity,
                    transform);


                yield return new WaitForSeconds(0.05f);
                currentCount--;
                float randomOffset = UnityEngine.Random.Range(-100, 100);
                Vector3 midlePos = Utils.GetCurvePoint(coinPosRect.position, globalMoneyPosRect.position, randomOffset);
                //coinTmp.transform.DOMove(globalMoneyPosRect.position, 1);
                Vector3[] path = new[] {midlePos, globalMoneyPosRect.position};
                coinTmp.transform.DOPath(path, 0.7f, PathType.CatmullRom, PathMode.Full3D);
                coinTmp.transform.DOScale(Vector3.zero, 0.7f);
                AudioManager.Instance.PlaySound("coins", maxSameSounds: currentCount);
            }


            startMoneyValue = endMoneyValue;
            endMoneyValue = 0;
            float startMoneyValueGlobal = MoneyManager.Instance.GetGameMoney();
            float endMoneyValueGlobal = (startMoneyValueGlobal + startMoneyValue);
            percent = 0;
            timeFactor = 1 / duration;
            while (percent < 1)
            {
                percent += Time.deltaTime * timeFactor;
                value = (int) Mathf.Lerp(startMoneyValue, endMoneyValue, Mathf.SmoothStep(0, 1, percent));
                int globalValue =
                    (int) Mathf.Lerp(startMoneyValueGlobal, endMoneyValueGlobal, Mathf.SmoothStep(0, 1, percent));
                levelMoneyText.text = "+" + value;
                gameMoneyText.text = globalValue.ToString();
                yield return null;
            }

            levelMoneyText.text = "0";
        }

        isAnimCountingWork = false;

        MoneyManager.Instance.AddGameMoney(moneyAdd);
    }
}