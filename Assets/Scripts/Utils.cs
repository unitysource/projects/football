﻿using UnityEngine;

public class Utils
{
    public static Vector3 GetCurvePoint(Vector2 pos1, Vector2 pos2, float offset)
    {
        Vector2 dir = (pos2 - pos1).normalized;
        Vector2 perpDir = Vector2.Perpendicular(dir);
        Vector2 midPoint = (pos1 + pos2) / 2f;
        Vector2 offsetPoint = midPoint + (perpDir * offset);

        return offsetPoint;
    }
}