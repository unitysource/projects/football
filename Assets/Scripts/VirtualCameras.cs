﻿using Cinemachine;
using UnityEngine;

public class VirtualCameras : MonoBehaviour
{
    public static VirtualCameras Instance { get; set; }

    [SerializeField] private CinemachineVirtualCamera runVirtualCamera;
    [SerializeField] private CinemachineVirtualCamera aimVirtualCamera;
    [SerializeField] private CinemachineVirtualCamera finishVirtualCamera;
    [SerializeField] private CinemachineImpulseSource impulseSource;
    [SerializeField] private SignalSourceAsset cickBallSignal;


    public Camera MainCamera { get; set; }
    private CinemachineBrain cinemachineBrain;
    private float durationSwichCamera = 1;
    private int priority = 10;

    private void Awake()
    {
        MainCamera = Camera.main;
        if (Instance == null)
            Instance = this;

        cinemachineBrain = MainCamera.gameObject.GetComponent<CinemachineBrain>();
        cinemachineBrain.m_DefaultBlend.m_Time = durationSwichCamera;
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.C))
        {
            CickBall();
        }
    }


    public void CickBall()
    {
        impulseSource.m_ImpulseDefinition.m_RawSignal = cickBallSignal;
        //impulseSource.m_ImpulseDefinition.m_TimeEnvelope.m_SustainTime = 0.2f;
        impulseSource.GenerateImpulse();
    }

    public void SetRunVirtualCamera(Transform follow, Transform lookAt)
    {
        CinemachineVirtualCamera v = Instantiate(runVirtualCamera, runVirtualCamera.transform.position,
            Quaternion.identity);

        // aimVirtualCamera.Priority = 10;
        // runVirtualCamera.m_Follow = follow;
        // runVirtualCamera.m_LookAt = lookAt;
        priority++;
        v.Priority = priority;
        v.m_Follow = follow;
        v.m_LookAt = lookAt;
    }

    public void SetAimVirtualCamera(Transform follow, Transform lookAt)
    {
        priority++;
        CinemachineVirtualCamera v = Instantiate(aimVirtualCamera, aimVirtualCamera.transform.position,
            Quaternion.identity);
        v.Priority = priority;
        v.m_Follow = follow;
        v.m_LookAt = lookAt;
    }


    public void SetFinishVirtualCamera(Transform follow, Transform lookAt)
    {
        priority++;
        finishVirtualCamera.Priority = priority;
        finishVirtualCamera.m_Follow = follow;
        finishVirtualCamera.m_LookAt = lookAt;
    }
}